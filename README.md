## Installation

```shell
$ git clone https://gitlab.com/slim-4-modular/auth-api.git

$ cd auth-api
$ composer install

$ bin/install-dev
```
> **Important !!!**
> 
> ![](./doc/connection-refused.png)
> 
> In my case, sometimes the installation process breaks during triggering migrations.   
> I suppose it has something to do with my local hardware limitations, but if this applies to you as well, wait +/- 30 sec and manually repeat the following command after installation in the terminal:
> 
> `$ docker-compose exec -u www-data auth-api php bin/paneric-migrations migrate -f AuthApi/Migrations/`

## DB schema

![](./doc/auth-db-schema.png)

## Example of working url

```
http://127.0.0.1:81/auth-api/actions/get
```

## Integration tests

```shell
$ bin/phpunit Tests/Integration
```

## Introduction

The core of this solution is based on two libraries:
1. `module-environment` - roughly speaking takes care for resources' paths of a particular module.
2. `component-module-api` - package of module's core classes and interfaces extended and/or implemented within the module.   

### The main idea

- folder `./AuthApi` stands for completely autonomous `module`, decoupled from application.
- application may consist of several `modules`
  - module can be easily removed from one application to another or become a starting point for a completely new service if became grown too much.
- `modules` may consist of other `modules->submodules` - `AuthApi` consists of  `Action`, `Credential`,...
- application loads resources only required by a particular `module`/`submodule`

