<?php

declare(strict_types=1);

namespace Paneric\AuthApi\AcnCrd\Interfaces;

use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface as BaseModuleConfigInterface;

interface AcnCrdConfigInterface extends BaseModuleConfigInterface
{
    public function getAllByCredential(): array;
}
