<?php

declare(strict_types=1);

namespace Paneric\AuthApi\AcnCrd\config;

use Paneric\AuthApi\AcnCrd\Interfaces\AcnCrdConfigInterface;
use Paneric\AuthApi\AcnCrd\Model\AcnCrdADAO;
use Paneric\ComponentModuleApi\Action\Config\ModuleConfigGhostTrait;
use PDO;

class ModuleConfig implements AcnCrdConfigInterface
{
    use ModuleConfigGhostTrait;

    protected string $table = 'action_privilege';
    protected string $prefix = 'acpv_';

    protected bool $stringId = false;

    protected string $adaoClass = AcnCrdADAO::class;

    protected string $query = "
        SELECT *
            FROM action_privilege AS acpv
                LEFT JOIN action AS acn ON acpv.acpv_action_id = acn.acn_id
                LEFT JOIN privilege AS prv ON acpv.acpv_privilege_id = prv.prv_id
                LEFT JOIN privilege_role AS pvrl ON acpv.acpv_privilege_id = pvrl.pvrl_privilege_id
                    LEFT JOIN role_credential AS rlcr ON pvrl.pvrl_role_id = rlcr.rlcr_role_id
                        LEFT JOIN credential AS crd ON rlcr.rlcr_credential_id = crd.crd_id
    ";

    public function repository(): array
    {
        return [
            'table' => $this->table,
            'dto_class' => $this->adaoClass,
            'select_query' => $this->query,
            'fetch_mode' => PDO::FETCH_CLASS,
        ];
    }

    public function getAllByCredential(): array
    {
        return [
            'fetch_mode' => PDO::FETCH_ASSOC,
            'query' => "
                SELECT CONCAT(prv_ref, '.', acn_ref) AS acpv_ref
                FROM action_privilege AS acpv
                    LEFT JOIN action AS acn ON acpv.acpv_action_id = acn.acn_id
                    LEFT JOIN privilege AS prv ON acpv.acpv_privilege_id = prv.prv_id
                    LEFT JOIN privilege_role AS pvrl ON acpv.acpv_privilege_id = pvrl.pvrl_privilege_id
                        LEFT JOIN role_credential AS rlcr ON pvrl.pvrl_role_id = rlcr.rlcr_role_id
                            LEFT JOIN credential AS crd ON rlcr.rlcr_credential_id = crd.crd_id
            ",
            'find_by_single_criteria' => function (string $field, int|string $value): array {
                return ['crd_' . $field => urldecode($value)];
            },
            'order_by_single' => function (string $orderByField = 'ref', string $order = 'ASC'): array {
                return ['prv_' . $orderByField => $order];
            },
        ];
    }
}
//PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, // DAO
//PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS, //ADAO
//PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
