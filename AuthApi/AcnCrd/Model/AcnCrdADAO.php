<?php

declare(strict_types=1);

namespace Paneric\AuthApi\AcnCrd\Model;

use Paneric\AuthApi\Action\Model\ActionDAO;
use Paneric\AuthApi\Credential\Model\CredentialDAO;
use Paneric\DataObject\ADAO;

class AcnCrdADAO extends ADAO
{
    protected ?int $id;

    protected ?ActionDAO $action;
    protected ?CredentialDAO $credential;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'acpv_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->action = new ActionDAO();
            $this->values = $this->action->hydrate($this->values);

            $this->credential = new CredentialDAO();
            $this->values = $this->credential->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAction(): ActionDAO
    {
        return $this->action;
    }
    public function getCredential(): CredentialDAO
    {
        return $this->credential;
    }


    public function setId(null|int|string $id): void
    {
        if ($id !== null && !is_int($id)) {
            $this->id = (int) $id;
            return;
        }
        $this->id = $id;
    }

    public function setAction(ActionDAO $action): void
    {
        $this->action = $action;
    }
    public function setCredential(CredentialDAO $credential): void
    {
        $this->credential = $credential;
    }
}
