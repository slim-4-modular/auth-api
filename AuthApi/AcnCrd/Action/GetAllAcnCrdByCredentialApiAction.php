<?php

declare(strict_types=1);

namespace Paneric\AuthApi\AcnCrd\Action;

use Paneric\AuthApi\AcnCrd\Interfaces\AcnCrdConfigInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\GetAllByApiActionInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\CSRTriad\Action;

class GetAllAcnCrdByCredentialApiAction extends Action implements GetAllByApiActionInterface
{
    protected ModuleRepositoryInterface $adapter;
    protected array $config;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        AcnCrdConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getAllByCredential();
    }

    public function __invoke(array $queryParams, string $field, string $value): array
    {
        $this->adapter->setSelectQuery($this->config['query']);
        $this->adapter->setFetchMode($this->config['fetch_mode']);

        return [
            'status' => 200,
            'body' => $this->getAllByCredential($queryParams, $field, urldecode($value)),
        ];
    }

    public function getAllByCredential(array $queryParams, string $field, string $value): array
    {
        $orderBySingle = $this->config['order_by_single'];
        $findBySingleCriteria = $this->config['find_by_single_criteria'];

        return $this->adapter->findBy(
            $findBySingleCriteria($field, $value),
            $orderBySingle($queryParams['field'] ?? 'id')
        );
    }
}
