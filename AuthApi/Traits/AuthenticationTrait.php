<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Traits;

trait AuthenticationTrait
{
    public function isAuthenticated(?array $credential, array $attributes): bool
    {
        if ($credential === null) {
            return false;
        }

        if (
            !$this->guard->verifyPassword(
                $attributes['password_hash'],
                $credential['body']['password_hash']
            )
        ) {
            return false;
        }

        return true;
    }
}
