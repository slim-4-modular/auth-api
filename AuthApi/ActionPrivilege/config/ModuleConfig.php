<?php

declare(strict_types=1);

namespace Paneric\AuthApi\ActionPrivilege\config;

use Paneric\AuthApi\ActionPrivilege\Model\ActionPrivilegeADAO;
use Paneric\AuthApi\ActionPrivilege\Model\ActionPrivilegeDAO;
use Paneric\AuthApi\ActionPrivilege\Model\ActionPrivilegeVLD;
use Paneric\ComponentModuleApi\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use PDO;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'action_privilege';
    protected string $prefix = 'acpv_';

    protected array $uniqueKeys = ['action_id', 'privilege_id'];//unique key

    protected string $adaoClass = ActionPrivilegeADAO::class;
    protected string $daoClass = ActionPrivilegeDAO::class;
    protected string $vldClass = ActionPrivilegeVLD::class;

    protected bool $stringId = false;

    protected string $query = "
        SELECT * 
        FROM action_privilege AS acpv
            LEFT JOIN action AS acn ON acpv.acpv_action_id = acn.acn_id
            LEFT JOIN privilege AS prv ON acpv.acpv_privilege_id = prv.prv_id
    ";

    public function repository(): array
    {
        return [
            'table' => $this->table,
            'dto_class' => $this->adaoClass,
            'select_query' => $this->query,
            'fetch_mode' => PDO::FETCH_CLASS,
        ];
    }
}
