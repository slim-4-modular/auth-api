<?php

declare(strict_types=1);

namespace Paneric\AuthApi\ActionPrivilege\Model;

trait ActionPrivilegeTrait
{
    protected ?int $actionId;
    protected ?int $privilegeId;


    public function getActionId(): ?int
    {
        return $this->actionId;
    }
    public function getPrivilegeId(): ?int
    {
        return $this->privilegeId;
    }


    public function setActionId(null|int|string $actionId): void
    {
        if ($actionId !== null && !is_int($actionId)) {
            $this->actionId = (int) $actionId;
            return;
        }
        $this->actionId = $actionId;
    }
    public function setPrivilegeId(null|int|string $privilegeId): void
    {
        if ($privilegeId !== null && !is_int($privilegeId)) {
            $this->privilegeId = (int) $privilegeId;
            return;
        }
        $this->privilegeId = $privilegeId;
    }
}
