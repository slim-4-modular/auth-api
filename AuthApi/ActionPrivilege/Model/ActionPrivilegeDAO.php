<?php

declare(strict_types=1);

namespace Paneric\AuthApi\ActionPrivilege\Model;

use Paneric\DataObject\DAO;

class ActionPrivilegeDAO extends DAO
{
    use ActionPrivilegeTrait;

    protected ?int $id = null;

    public function __construct()
    {
        $this->prefix = 'acpv_';

        $this->setMaps();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(null|int|string $id): void
    {
        if ($id !== null && !is_int($id)) {
            $this->id = (int) $id;
            return;
        }
        $this->id = $id;
    }
}
