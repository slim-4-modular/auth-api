<?php

declare(strict_types=1);

use App\config\JWTConfig;
use Paneric\AuthApi\AcnCrd\Action\GetAllAcnCrdByCredentialApiAction;
use Paneric\AuthApi\AcnCrd\config\ModuleConfig as AcnCrdConfig;
use Paneric\AuthApi\Credential\Middleware\ActivateMiddleware;
use Paneric\AuthApi\Credential\Middleware\RequestActivationResetMiddleware;
use Paneric\AuthApi\Credential\Middleware\RequestPasswordResetMiddleware;
use Paneric\AuthApi\Credential\Middleware\ResetApiTokenMiddleware;
use Paneric\AuthApi\Credential\Middleware\ResetPasswordMiddleware;
use Paneric\AuthApi\Credential\Middleware\UnregisterMiddleware;
use Paneric\ComponentModuleApi\Action\GetOneByApiAction;
use Paneric\ComponentModuleApi\Infrastructure\ModuleRepository;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\DBAL\Manager;
use Paneric\JWT\JWTEncoder;
use Psr\Container\ContainerInterface;

return [
    RequestActivationResetMiddleware::class => static function (
        ContainerInterface $c
    ): RequestActivationResetMiddleware {
        return new RequestActivationResetMiddleware(
            $c->get(GetOneByApiAction::class),
            $c->get(GuardInterface::class)
        );
    },

    ActivateMiddleware::class => static function (ContainerInterface $c): ActivateMiddleware {
        $mailerServiceConfig = $c->get('mailer-service-config');
        $resetDelay = $mailerServiceConfig['activation_reset_time_delay'];
        return new ActivateMiddleware(
            $c->get(GetOneByApiAction::class),
            $c->get(GuardInterface::class),
            $resetDelay
        );
    },

    RequestPasswordResetMiddleware::class => static function (
        ContainerInterface $c
    ): RequestPasswordResetMiddleware {
        return new RequestPasswordResetMiddleware(
            $c->get(GetOneByApiAction::class),
            $c->get(GuardInterface::class)
        );
    },

    ResetPasswordMiddleware::class => static function (
        ContainerInterface $c
    ): ResetPasswordMiddleware {
        $mailerServiceConfig = $c->get('mailer-service-config');
        $resetDelay = $mailerServiceConfig['password_reset_time_delay'];
        return new ResetPasswordMiddleware(
            $c->get(GetOneByApiAction::class),
            $c->get(GuardInterface::class),
            $resetDelay
        );
    },

    'acn_crd_module_config' => static function (ContainerInterface $c): ModuleConfigInterface {
        return new AcnCrdConfig();
    },
    'acn_crd_module_repository' => static function (ContainerInterface $c): ModuleRepositoryInterface {
        return new ModuleRepository(
            $c->get(Manager::class),
            $c->get('acn_crd_module_config')
        );
    },
    GetAllAcnCrdByCredentialApiAction::class => static function (
        ContainerInterface $c
    ): GetAllAcnCrdByCredentialApiAction {
        return new GetAllAcnCrdByCredentialApiAction(
            $c->get('acn_crd_module_repository'),
            $c->get('acn_crd_module_config')
        );
    },
    JWTEncoder::class => static function (ContainerInterface $c): JWTEncoder {
        return new JWTEncoder(
            $c->get(GuardInterface::class),
            $c->get(JWTConfig::class)
        );
    },
    ResetApiTokenMiddleware::class => static function (ContainerInterface $c): ResetApiTokenMiddleware {
        return new ResetApiTokenMiddleware(
            $c->get(GetOneByApiAction::class),
            $c->get(GetAllAcnCrdByCredentialApiAction::class),
            $c->get(JWTEncoder::class),
            $c->get(GuardInterface::class)
        );
    },

    UnregisterMiddleware::class => static function (ContainerInterface $c): UnregisterMiddleware {
        return new UnregisterMiddleware(
            $c->get(GetOneByApiAction::class),
            $c->get(GuardInterface::class)
        );
    },
];
