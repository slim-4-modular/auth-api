<?php

declare(strict_types=1);

use Paneric\AuthApi\Credential\Action\RegisterApiAction as RegisterCredentialApiAction;
use Paneric\AuthApi\Credential\config\ModuleConfig;
use Paneric\AuthApi\Credential\config\TransactionApiConfig;
use Paneric\AuthApi\Credential\Interfaces\Transaction\RegisterApiTransactionInterface;
use Paneric\AuthApi\Credential\Interfaces\Transaction\UnregisterApiTransactionInterface;
use Paneric\AuthApi\Credential\Transaction\RegisterApiTransaction;
use Paneric\AuthApi\Credential\Transaction\UnregisterApiTransaction;
use Paneric\AuthApi\Role\config\ModuleConfig as RoleConfig;
use Paneric\AuthApi\RoleCredential\config\ModuleConfig as RoleCredentialConfig;
use Paneric\ComponentModuleApi\Action\CreateApiAction;
use Paneric\ComponentModuleApi\Action\DeleteApiAction;
use Paneric\ComponentModuleApi\Action\DeleteByApiAction;
use Paneric\ComponentModuleApi\Action\GetOneByApiAction;
use Paneric\ComponentModuleApi\Infrastructure\ModulePersister;
use Paneric\ComponentModuleApi\Infrastructure\ModuleRepository;
use Paneric\ComponentModuleApi\Interfaces\Action\CreateApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\DeleteByApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\GetOneByApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\DBAL\Manager;
use Paneric\Interfaces\Guard\GuardInterface;
use Psr\Container\ContainerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

return array_merge(
    (array) require './../vendor/paneric/component-module-api/src/config/dependencies/cam.php',
    [
        ModuleConfigInterface::class => static function (): ModuleConfig {
            return new ModuleConfig();
        },


        'role_module_config' => static function (ContainerInterface $c): ModuleConfigInterface {
            return new RoleConfig();
        },
        'role_module_repository' => static function (ContainerInterface $c): ModuleRepositoryInterface {
            return new ModuleRepository(
                $c->get(Manager::class),
                $c->get('role_module_config')
            );
        },
        'get_one_role_by_api_action' => static function (ContainerInterface $c): GetOneByApiActionInterface {
            return new GetOneByApiAction(
                $c->get('role_module_repository'),
                $c->get('role_module_config')
            );
        },


        'role_credential_module_config' => static function (ContainerInterface $c): ModuleConfigInterface {
            return new RoleCredentialConfig();
        },
        'role_credential_module_persister' => static function (ContainerInterface $c): ModulePersisterInterface {
            return new ModulePersister(
                $c->get(Manager::class),
                $c->get('role_credential_module_config')
            );
        },
        'create_role_credential_api_action' => static function (ContainerInterface $c): CreateApiActionInterface {
            return new CreateApiAction(
                $c->get('role_credential_module_persister'),
                $c->get('role_credential_module_config'),
                $c->get(GuardInterface::class),
                $c->get(EventDispatcherInterface::class),
            );
        },


        RegisterApiTransaction::class => static function (ContainerInterface $c): RegisterApiTransactionInterface {
            return new RegisterApiTransaction(
                $c->get(Manager::class),
                $c->get('get_one_role_by_api_action'),
                $c->get(RegisterCredentialApiAction::class),
                $c->get('create_role_credential_api_action'),
                $c->get(TransactionApiConfig::class),
            );
        },


        'delete_role_credential_by_api_action' => static function (ContainerInterface $c): DeleteByApiActionInterface {
            return new DeleteByApiAction(
                $c->get('role_credential_module_persister'),
                $c->get('role_credential_module_config'),
                $c->get(EventDispatcherInterface::class),
            );
        },


        UnregisterApiTransaction::class => static function (ContainerInterface $c): UnregisterApiTransactionInterface {
            return new UnregisterApiTransaction(
                $c->get(Manager::class),
                $c->get('delete_role_credential_by_api_action'),
                $c->get(DeleteApiAction::class),
                $c->get(TransactionApiConfig::class),
            );
        },
    ]
);
