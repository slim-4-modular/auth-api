<?php

declare(strict_types=1);

use Paneric\AuthApi\EventSubscriber\Service\MailerService;
use Paneric\Interfaces\Guard\GuardInterface;
use Psr\Container\ContainerInterface;

return [
    MailerService::class => static function (ContainerInterface $c): MailerService {
        return new MailerService(
            $c->get(GuardInterface::class),
            $c->get('mailer-service-config'),
            $c->get('local')
        );
    },
];
