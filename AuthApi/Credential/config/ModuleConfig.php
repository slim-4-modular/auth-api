<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\config;

use Paneric\AuthApi\Credential\Model\CredentialVLD;
use Paneric\AuthApi\Credential\Model\CredentialDAO;
use Paneric\ComponentModuleApi\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'credential';
    protected string $prefix = 'crd_';

    protected array $uniqueKeys = ['email'];//unique keys

    protected string $daoClass = CredentialDAO::class;
    protected string $vldClass = CredentialVLD::class;

    protected bool $stringId = true;
}
