<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\config;

use Paneric\AuthApi\Credential\Model\CredentialDAO;
use Paneric\AuthApi\Role\Model\RoleDAO;
use Paneric\AuthApi\RoleCredential\Model\RoleCredentialDAO;
use Paneric\Interfaces\Config\ConfigInterface;
use PDO;

class TransactionApiConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'register' => [
                'role' => [
                    'table' => 'role',
                    'dao_class' => RoleDAO::class,
                    'fetch_mode' => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
                ],
                'credential' => [
                    'table' => 'credential',
                    'dao_class' => CredentialDAO::class,
                    'fetch_mode' => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
                ],
                'role_credential' => [
                    'table' => 'role_credential',
                    'dao_class' => RoleCredentialDAO::class,
                    'fetch_mode' => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
                ],
            ],
            'unregister' => [
                'credential' => [
                    'table' => 'credential',
                    'dao_class' => CredentialDAO::class,
                    'fetch_mode' => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
                ],
                'role_credential' => [
                    'table' => 'role_credential',
                    'dao_class' => RoleCredentialDAO::class,
                    'fetch_mode' => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
                ],
            ],
        ];
    }
}
