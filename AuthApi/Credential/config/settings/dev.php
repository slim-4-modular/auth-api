<?php

declare(strict_types=1);

return [
    'mailer-service-config' => [
        'random_string_length' => 128,

        'sender' => 'support@' . $_ENV['APP_DOMAIN'],

        'smtp_user' => '23f054dd15af2f',
        'smtp_password' => '15474543b4f1d6',

        'dsn' => 'smtp://%s:%s@auth-api-mailhog:1025',

        'activation_reset_url' => 'http://127.0.0.1:82/auth-apc/account/activate',
        'activation_reset_time_delay' => '24',

        'password_reset_url' => 'http://127.0.0.1:82/auth-apc/account/reset-password',
        'password_reset_time_delay' => '24',

        'mail_content' => [
            'activation_reset_email_subject' => [
                'en' => 'Activation reset automatic message.'
            ],
            'activation_reset_email_message' => [
                'en' => 'Please click the following link within %sh to activate your account: %s/%s/%s .'
            ],
            'password_reset_email_subject' => [
                'en' => 'Password reset automatic message.'
            ],
            'password_reset_email_message' => [
                'en' => 'Please click the following link within %sh to reset your account password: %s/%s/%s .'
            ],
        ],
    ],
];
