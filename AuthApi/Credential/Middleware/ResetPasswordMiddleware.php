<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Middleware;

use DateTimeImmutable;
use Exception;
use Paneric\ComponentModuleApi\Interfaces\Action\GetOneByApiActionInterface;
use Paneric\Interfaces\Guard\GuardInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ResetPasswordMiddleware implements MiddlewareInterface
{
    public function __construct(
        protected GetOneByApiActionInterface $getOneCredentialBy,
        protected GuardInterface $guard,
        protected string $resetDelay
    ) {
    }

    /**
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $routeArguments = $request->getAttribute('route_arguments');
        $attributes = $request->getParsedBody();

        /** @var null|array $credential */
        $credential = ($this->getOneCredentialBy)('id', $routeArguments['id']);

        if ($credential === null) {
            return $handler->handle($request->withParsedBody([]));
        }

        $credential = $credential['body'];

        $resetId = $credential['password_reset_id'];
        if ($resetId === null) {
            return $handler->handle($request->withParsedBody([]));
        }

        if (!$this->guard->verifyHash($this->guard->hash($resetId), $routeArguments['hash'])) {
            return $handler->handle($request->withParsedBody([]));
        }

        if (!$this->verifyDelay($credential['updated_at'])) {
            return $handler->handle($request->withParsedBody([]));
        }

        $credentialId = $credential['id'];
        $request = $request->withAttribute('credential_id', $credentialId);
        unset($credential['id'], $credential['updated_at']);
        $credential['password_hash'] = $attributes['password_hash'];

        return $handler->handle($request->withParsedBody($credential));
    }

    /**
     * @throws Exception
     */
    private function verifyDelay(string $date): bool
    {
        $requestDate = new DateTimeImmutable($date);
        $delayDate = $requestDate->add(new \DateInterval(sprintf('PT%sH', $this->resetDelay)));
        $now = new DateTimeImmutable();

        return $now < $delayDate;
    }
}
