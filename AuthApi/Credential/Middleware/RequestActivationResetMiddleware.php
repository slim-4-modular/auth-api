<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Middleware;

use Paneric\AuthApi\Traits\AuthenticationTrait;
use Paneric\ComponentModuleApi\Interfaces\Action\GetOneByApiActionInterface;
use Paneric\Interfaces\Guard\GuardInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RequestActivationResetMiddleware implements MiddlewareInterface
{
    use AuthenticationTrait;

    public function __construct(
        protected GetOneByApiActionInterface $getOneCredentialBy,
        protected GuardInterface $guard
    ) {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $attributes = $request->getParsedBody();
        /** @var null|array $credential */
        $credential = ($this->getOneCredentialBy)('email', $attributes['email']);

        if (!$this->isAuthenticated($credential, $attributes)) {
            return $handler->handle($request->withParsedBody([]));
        }

        if ($credential['body']['is_active']) {
            return $handler->handle($request->withParsedBody([]));
        }

        $credential = $credential['body'];

        $credentialId = $credential['id'];
        $request = $request->withAttribute('credential_id', $credentialId);
        unset($credential['id'], $credential['updated_at']);

        return $handler->handle($request->withParsedBody($credential));
    }
}
