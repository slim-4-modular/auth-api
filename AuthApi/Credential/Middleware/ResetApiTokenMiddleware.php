<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Middleware;

use Paneric\AuthApi\Traits\AuthenticationTrait;
use Paneric\ComponentModuleApi\Interfaces\Action\GetAllByApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\GetOneByApiActionInterface;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\JWT\JWTEncoder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ResetApiTokenMiddleware implements MiddlewareInterface
{
    use AuthenticationTrait;

    public function __construct(
        protected GetOneByApiActionInterface $getOneCredentialBy,
        protected GetAllByApiActionInterface $getAllAcnCrdByCredential,
        protected JWTEncoder $jwtEncoder,
        protected GuardInterface $guard
    ) {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $attributes = $request->getParsedBody();

        if (
            empty($attributes['email']) ||
            empty($attributes['password_hash']) ||
            empty($attributes['audience'])
        ) {
            return $handler->handle($request->withParsedBody([]));
        }

        /** @var null|array $credential */
        $credential = ($this->getOneCredentialBy)('email', $attributes['email']);

        if (isset($credential['error'])) {
            return $handler->handle($request->withParsedBody([]));
        }

        if (!$this->isAuthenticated($credential, $attributes)) {
            return $handler->handle($request->withParsedBody([]));
        }

        $credential = $credential['body'];
        $credentialId = $credential['id'];
        unset($credential['id'], $credential['updated_at']);

        $request = $request->withAttribute('credential_id', $credentialId);

        if (!$credential['is_active']) {
            return $handler->handle($request->withParsedBody($credential));
        }

        $credential['api_token'] = $this->prepareApiToken($attributes);

        return $handler->handle($request->withParsedBody($credential));
    }

    protected function prepareApiToken(array $attributes): string
    {
        $credentialActions = ($this->getAllAcnCrdByCredential)([], 'email', $attributes['email']);
        $credentialActions = $this->prepareCredentialActions($credentialActions['body']);

        return ($this->jwtEncoder)($credentialActions, $attributes['audience']);
    }

    protected function prepareCredentialActions(array $actionsOfCredential): array
    {
        $actions = [];
        foreach ($actionsOfCredential as $action) {
            $actions[] = $action['acpv_ref'];
        }

        return $actions;
    }
}
