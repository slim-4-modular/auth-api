<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\EventSubscriber;

use Paneric\AuthApi\Credential\Infrastructure\Event\ActivateFailureEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\ActivateSuccessEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\BeforeActivateEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\BeforeRegisterEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\BeforeRequestActivationResetEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\BeforeRequestPasswordResetEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\RegisterFailureEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\RegisterSuccessEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\RequestActivationResetFailureEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\RequestActivationResetSuccessEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\RequestPasswordResetFailureEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\RequestPasswordResetSuccessEvent;
use Paneric\AuthApi\Credential\Model\CredentialDAO;
use Paneric\AuthApi\EventSubscriber\Service\MailerService;
use Paneric\AuthApi\EventSubscriber\Traits\EventSubscriberMailerTrait;
use Paneric\Logger\EventLogger;

class CredentialEventSubscriber
{
    use EventSubscriberMailerTrait;

    public function __construct(
        readonly private EventLogger $eventLogger,
        readonly protected MailerService $mailerService
    ) {
    }

    public function onBeforeRequestActivationReset(BeforeRequestActivationResetEvent $event): void
    {
        $this->setProcessId($event, 'activation');
    }
    public function onRequestActivationResetSuccess(RequestActivationResetSuccessEvent $event): void
    {
        $this->sendProcessLink($event, 'activation');
    }
    public function onRequestActivationResetFailure(RequestActivationResetFailureEvent $event): void
    {
        /** @var CredentialDAO $credential */
        $credential = $event->getData();

        $this->eventLogger->info(sprintf(
            'Credential request activation reset failure for %s.',
            $credential->getEmail()
        ));
    }


    public function onBeforeActivate(BeforeActivateEvent $event): void
    {
    }
    public function onActivateSuccess(ActivateSuccessEvent $event): void
    {
    }
    public function onActivateFailure(ActivateFailureEvent $event): void
    {
    }


    public function onBeforeRequestPasswordReset(BeforeRequestPasswordResetEvent $event): void
    {
        $this->setProcessId($event, 'password');
    }
    public function onRequestPasswordResetSuccess(RequestPasswordResetSuccessEvent $event): void
    {
        $this->sendProcessLink($event, 'password');
    }
    public function onRequestPasswordResetFailure(RequestPasswordResetFailureEvent $event): void
    {
        /** @var CredentialDAO $credential */
        $credential = $event->getData();

        $this->eventLogger->info(sprintf(
            'Credential request password reset failure for %s.',
            $credential->getEmail()
        ));
    }


    public function onBeforeRegister(BeforeRegisterEvent $event): void
    {
        $this->setProcessId($event, 'activation');
    }
    public function onRegisterSuccess(RegisterSuccessEvent $event): void
    {
        $this->sendProcessLink($event, 'activation');
    }
    public function onRegisterFailure(RegisterFailureEvent $event): void
    {
        /** @var CredentialDAO $credential */
        $credential = $event->getData();

        $this->eventLogger->info(sprintf(
            'Credential registration failure for %s.',
            $credential->getEmail()
        ));
    }
}
