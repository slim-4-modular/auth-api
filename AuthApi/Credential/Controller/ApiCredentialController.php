<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Controller;

use Paneric\AuthApi\Credential\Interfaces\Action\ActivateApiActionInterface;
use Paneric\AuthApi\Credential\Interfaces\Action\RequestActivationResetApiActionInterface;
use Paneric\AuthApi\Credential\Interfaces\Action\RequestPasswordResetApiActionInterface;
use Paneric\AuthApi\Credential\Interfaces\Action\ResetApiTokenApiActionInterface;
use Paneric\AuthApi\Credential\Interfaces\Action\ResetPasswordApiActionInterface;
use Paneric\AuthApi\Credential\Interfaces\Transaction\RegisterApiTransactionInterface;
use Paneric\AuthApi\Credential\Interfaces\Transaction\UnregisterApiTransactionInterface;
use Paneric\ComponentModuleApi\Controller\ApiModuleController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ApiCredentialController extends ApiModuleController
{
    public function requestActivationReset(
        Request $request,
        Response $response,
        RequestActivationResetApiActionInterface $action
    ): Response {
        return ($this->responder)(
            $response,
            $action($request->getParsedBody(), $request->getAttribute('credential_id'))
        );
    }

    public function activate(
        Request $request,
        Response $response,
        ActivateApiActionInterface $action
    ): Response {
        return ($this->responder)(
            $response,
            $action($request->getParsedBody(), $request->getAttribute('credential_id'))
        );
    }

    public function requestPasswordReset(
        Request $request,
        Response $response,
        RequestPasswordResetApiActionInterface $action
    ): Response {
        return ($this->responder)(
            $response,
            $action($request->getParsedBody(), $request->getAttribute('credential_id'))
        );
    }

    public function resetPassword(
        Request $request,
        Response $response,
        ResetPasswordApiActionInterface $action
    ): Response {
        return ($this->responder)(
            $response,
            $action(
                $request->getParsedBody(),
                $request->getAttribute('route_name'),
                $request->getAttribute('credential_id')
            )
        );
    }

    public function resetApiToken(
        Request $request,
        Response $response,
        ResetApiTokenApiActionInterface $action
    ): Response {
        return ($this->responder)(
            $response,
            $action($request->getParsedBody(), $request->getAttribute('credential_id'))
        );
    }

    public function register(
        Request $request,
        Response $response,
        RegisterApiTransactionInterface $transaction
    ): Response {
        return ($this->responder)(
            $response,
            $transaction($request->getAttribute('route_name'), $request->getParsedBody())
        );
    }

    public function unregister(
        Request $request,
        Response $response,
        UnregisterApiTransactionInterface $transaction
    ): Response {
        return ($this->responder)(
            $response,
            $transaction($request->getAttribute('credential_id'))
        );
    }
}
