<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Model;

use Paneric\ComponentModuleApi\Model\Interfaces\ValidatorInterface;
use Paneric\ComponentModuleApi\Model\Validator;
use Webmozart\Assert\Assert;
use Webmozart\Assert\InvalidArgumentException;

class CredentialVLD extends Validator implements ValidatorInterface
{
    protected array $routeValidators = [
        'auth.credential.add' => ['email', 'gsm', 'passwordHash', 'terms'],
        'auth.credentials.add' => ['email', 'gsm', 'passwordHash', 'terms'],
        'auth.credential.edit' => ['email', 'gsm', 'passwordHash'],
        'auth.credentials.edit' => ['email', 'gsm', 'passwordHash'],
        'auth.credential.register' => ['email', 'gsm', 'passwordHash', 'passwordRepeat', 'terms'],
        'auth.credential.reset-password' => ['email', 'gsm', 'passwordHash', 'passwordRepeat'],
    ];

    protected function emailValidator(array $attributes, int $index = null): void
    {
        try {
            if (empty($attributes['email'])) {
                throw new InvalidArgumentException('required');
            }
            $value = trim($attributes['email']);
            Assert::email($value, 'invalid_email');
        } catch (InvalidArgumentException $e) {
            $this->setMessages('email', $e->getMessage(), $index);
        }
    }

    protected function gsmValidator(array $attributes, int $index = null): void
    {
        try {
            if (empty($attributes['gsm'])) {
                throw new InvalidArgumentException('required');
            }
            $value = trim($attributes['gsm']);
        } catch (InvalidArgumentException $e) {
            $this->setMessages('gsm', $e->getMessage(), $index);
        }
    }

    protected function passwordHashValidator(array $attributes, int $index = null): void
    {
        try {
            if (empty($attributes['password_hash'])) {
                throw new InvalidArgumentException('required');
            }
            $value = trim($attributes['password_hash']);
        } catch (InvalidArgumentException $e) {
            $this->setMessages('password_hash', $e->getMessage(), $index);
        }
    }

    protected function passwordRepeatValidator(array $attributes, int $index = null): void
    {
        try {
            if (empty($attributes['password_repeat'])) {
                throw new InvalidArgumentException('required');
            }
            $value = trim($attributes['password_repeat']);
            Assert::true($value === $attributes['password_hash'], 'not_same_as');
        } catch (InvalidArgumentException $e) {
            $this->setMessages('password_repeat', $e->getMessage(), $index);
        }
    }

    protected function termsValidator(array $attributes, int $index = null): void
    {
        try {
            if (empty($attributes['terms'])) {
                throw new InvalidArgumentException('required');
            }
            $value = $attributes['terms'];
            Assert::true((int) $value === 1, 'terms_not_accepted');
        } catch (InvalidArgumentException $e) {
            $this->setMessages('terms', $e->getMessage(), $index);
        }
    }
}
