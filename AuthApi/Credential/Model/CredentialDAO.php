<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Model;

use Paneric\DataObject\DAO;

class CredentialDAO extends DAO
{
    use CredentialTrait;

    protected null|int|string $id = null;

    protected string $email;
    protected string $gsm;
    protected string $passwordHash;
    protected string $passwordRepeat;
    protected int $terms = 0;
    protected int $isActive = 0;
    protected ?string $apiToken = null;
    protected ?string $activationResetId = null;
    protected ?string $passwordResetId = null;

    public function __construct()
    {
        $this->prefix = 'crd_';

        $this->setMaps();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
    public function getGsm(): string
    {
        return $this->gsm;
    }
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }
    public function getPasswordRepeat(): string
    {
        return $this->passwordRepeat;
    }
    public function getApiToken(): ?string
    {
        return $this->apiToken;
    }
    public function getTerms(): int
    {
        return $this->terms;
    }
    public function getIsActive(): int
    {
        return $this->isActive;
    }
    public function getActivationResetId(): ?string
    {
        return $this->activationResetId;
    }
    public function getPasswordResetId(): ?string
    {
        return $this->passwordResetId;
    }


    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
    public function setGsm(string $gsm): void
    {
        $this->gsm = $gsm;
    }
    public function setPasswordHash(string $passwordHash): void
    {
        $this->passwordHash = $passwordHash;
    }
    public function setPasswordRepeat(string $passwordRepeat): void
    {
        $this->passwordRepeat = $passwordRepeat;
    }
    public function setApiToken(?string $apiToken): void
    {
        $this->apiToken = $apiToken;
    }
    public function setTerms(int|string $terms): void
    {
        $this->terms = (int) $terms;
    }
    public function setIsActive(int|string $isActive): void
    {
        $this->isActive = (int) $isActive;
    }
    public function setActivationResetId(?string $activationResetId): void
    {
        $this->activationResetId = $activationResetId;
    }
    public function setPasswordResetId(?string $passwordResetId): void
    {
        $this->passwordResetId = $passwordResetId;
    }


    public function unsetPasswordRepeat(): void
    {
        unset($this->passwordRepeat);
    }
}
