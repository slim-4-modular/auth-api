<?php

declare(strict_types=1);

use Paneric\AuthApi\Credential\Action\ActivateApiAction;
use Paneric\AuthApi\Credential\Action\RequestActivationResetApiAction;
use Paneric\AuthApi\Credential\Action\RequestPasswordResetApiAction;
use Paneric\AuthApi\Credential\Action\ResetApiTokenApiAction;
use Paneric\AuthApi\Credential\Action\ResetPasswordApiAction;
use Paneric\AuthApi\Credential\Controller\ApiCredentialController;
use Paneric\AuthApi\Credential\Middleware\ActivateMiddleware;
use Paneric\AuthApi\Credential\Middleware\RequestActivationResetMiddleware;
use Paneric\AuthApi\Credential\Middleware\RequestPasswordResetMiddleware;
use Paneric\AuthApi\Credential\Middleware\ResetApiTokenMiddleware;
use Paneric\AuthApi\Credential\Middleware\ResetPasswordMiddleware;
use Paneric\AuthApi\Credential\Middleware\UnregisterMiddleware;
use Paneric\AuthApi\Credential\Transaction\RegisterApiTransaction;
use Paneric\AuthApi\Credential\Transaction\UnregisterApiTransaction;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require './../vendor/paneric/component-module-api/src/bootstrap/routes.php';

if (isset($slim, $container, $proxyPrefix, $module)) {
    $routePrefix = '/' . $module;
    $routeNamePrefix = $module;

    if (!empty($proxyPrefix)) {
        $routePrefix = $proxyPrefix  . '/' . $module;
        $routeNamePrefix = str_replace('/', '', $proxyPrefix) . '.' . $module;
        $routeNamePrefix = str_replace('-api', '', $routeNamePrefix);
    }

    try {
        $slim->post($routePrefix . '/request-activation-reset', function (Request $request, Response $response) {
            return $this->get(ApiCredentialController::class)->requestActivationReset(
                $request,
                $response,
                $this->get(RequestActivationResetApiAction::class)
            );
        })->setName($routeNamePrefix . '.request-activation-reset')
            ->addMiddleware($container->get(RequestActivationResetMiddleware::class));

        $slim->get($routePrefix . '/activate/{id}/{hash}', function (Request $request, Response $response) {
            return $this->get(ApiCredentialController::class)->activate(
                $request,
                $response,
                $this->get(ActivateApiAction::class)
            );
        })->setName($routeNamePrefix . '.activate')
            ->addMiddleware($container->get(ActivateMiddleware::class));


        $slim->post($routePrefix . '/request-password-reset', function (Request $request, Response $response) {
            return $this->get(ApiCredentialController::class)->requestPasswordReset(
                $request,
                $response,
                $this->get(RequestPasswordResetApiAction::class)
            );
        })->setName($routeNamePrefix . '.request-password-reset')
            ->addMiddleware($container->get(RequestPasswordResetMiddleware::class));

        $slim->post($routePrefix . '/reset-password/{id}/{hash}', function (Request $request, Response $response) {
            return $this->get(ApiCredentialController::class)->resetPassword(
                $request,
                $response,
                $this->get(ResetPasswordApiAction::class)
            );
        })->setName($routeNamePrefix . '.reset-password')
            ->addMiddleware($container->get(ResetPasswordMiddleware::class));


        $slim->post($routePrefix . '/reset-api-token', function (Request $request, Response $response) {
            return $this->get(ApiCredentialController::class)->resetApiToken(
                $request,
                $response,
                $this->get(ResetApiTokenApiAction::class)
            );
        })->setName($routeNamePrefix . '.reset-api-token')
            ->addMiddleware($container->get(ResetApiTokenMiddleware::class));


        $slim->post($routePrefix . '/register', function (Request $request, Response $response) {
            return $this->get(ApiCredentialController::class)->register(
                $request,
                $response,
                $this->get(RegisterApiTransaction::class)
            );
        })->setName($routeNamePrefix . '.register');

        $slim->post(
            $routePrefix . '/unregister',
            function (Request $request, Response $response) {
                return $this->get(ApiCredentialController::class)->unregister(
                    $request,
                    $response,
                    $this->get(UnregisterApiTransaction::class)
                );
            }
        )->setName($routeNamePrefix . '.unregister')
            ->addMiddleware($container->get(UnregisterMiddleware::class));
    } catch (NotFoundExceptionInterface | ContainerExceptionInterface $e) {
    }
}
