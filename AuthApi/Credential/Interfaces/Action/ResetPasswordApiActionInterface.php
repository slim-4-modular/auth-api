<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Interfaces\Action;

interface ResetPasswordApiActionInterface
{
    public function __invoke(array $attributes, string $routeName, ?string $id = null): ?array;
}
