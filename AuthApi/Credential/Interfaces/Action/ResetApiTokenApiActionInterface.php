<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Interfaces\Action;

interface ResetApiTokenApiActionInterface
{
    public function __invoke(array $attributes, ?string $id = null): array;
}
