<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Interfaces\Action;

interface RegisterApiActionInterface
{
    public function __invoke(array $attributes, string $routeName): ?array;
}
