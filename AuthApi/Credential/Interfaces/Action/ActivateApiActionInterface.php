<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Interfaces\Action;

interface ActivateApiActionInterface
{
    public function __invoke(array $attributes, ?string $id = null): ?array;
}
