<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Interfaces\Transaction;

interface UnregisterApiTransactionInterface
{
    public function __invoke(string $credentialId): array;
}
