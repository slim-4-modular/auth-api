<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Interfaces\Transaction;

interface RegisterApiTransactionInterface
{
    public function __invoke(string $routeName, ?array $attributes): array;
}
