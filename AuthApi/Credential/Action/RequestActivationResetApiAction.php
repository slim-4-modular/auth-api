<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Action;

use Paneric\AuthApi\Credential\Infrastructure\Event\BeforeRequestActivationResetEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\RequestActivationResetFailureEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\RequestActivationResetSuccessEvent;
use Paneric\AuthApi\Credential\Interfaces\Action\RequestActivationResetApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

class RequestActivationResetApiAction implements RequestActivationResetApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->adapter = $adapter;
        $this->config = $config->update();
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(array $attributes, ?string $id = null): ?array
    {
        if ($id === null) {
            return [
                'status' => 405,
                'error' => 'request_from_active_or_non_existing_user',
                'body' => ['email' => $attributes['email'] ?? '']
            ];
        }

        $dto = new $this->config['dto_class']();
        $dto->hydrate($attributes);

        $findOneByCriteria = $this->config['find_one_by_criteria'];
        $updateUniqueCriteria = $this->config['update_unique_criteria'];

        $this->eventDispatcher->dispatch(new BeforeRequestActivationResetEvent($dto));
        $updateResult = $this->adapter->updateUnique(
            $findOneByCriteria($dto, $id),
            $updateUniqueCriteria($id),
            $dto
        );
        $dto->setId($id);

        if ($updateResult !== null) {
            $this->eventDispatcher->dispatch(new RequestActivationResetSuccessEvent($dto));
            return [
                'status' => 200,
                'body' => ['email' => $attributes['email']]
            ];
        }

        $this->eventDispatcher->dispatch(new RequestActivationResetFailureEvent($dto));
        return [
            'status' => 400,
            'error' => 'db_update_error',
            'body' => ['email' => $attributes['email']]
        ];
    }
}
