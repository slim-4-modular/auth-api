<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Action;

use Paneric\AuthApi\Credential\Infrastructure\Event\BeforeRegisterEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\RegisterFailureEvent;
use Paneric\AuthApi\Credential\Infrastructure\Event\RegisterSuccessEvent;
use Paneric\AuthApi\Credential\Interfaces\Action\RegisterApiActionInterface;
use Paneric\ComponentModuleApi\Exceptions\ValidationException;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

class RegisterApiAction implements RegisterApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected GuardInterface $guard;
    protected EventDispatcherInterface $eventDispatcher;

    protected int $status;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        GuardInterface $guard,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->adapter = $adapter;
        $this->config = $config->create();
        $this->guard = $guard;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(array $attributes, string $routeName): ?array
    {
        $dto = new $this->config['dto_class']();
        $dto->hydrate($attributes);

        $createUniqueCriteria = $this->config['create_unique_criteria'];

        try {
            (new $this->config['vld_class']())($dto, $routeName);

            $dto->unsetPasswordRepeat();

            $stringId = $this->config['string_id'];

            if ($stringId) {
                $dto->setId($this->guard->setUniqueId());
            }

            $dto->setPasswordHash(
                $this->guard->hashPassword($dto->getPasswordHash())
            );

            $this->eventDispatcher->dispatch(new BeforeRegisterEvent($dto));
            $createResult = $this->adapter->createUnique($createUniqueCriteria($attributes), $dto);

            if ($createResult !== null) {
                $this->eventDispatcher->dispatch(new RegisterSuccessEvent($dto));
                return [
                    'status' => 201,
                    'body' => [
                        'id' => $stringId ? $dto->getId() : $createResult,
                    ],
                ];
            }

            $this->eventDispatcher->dispatch(new RegisterFailureEvent($dto));

            unset($attributes['password_hash']);

            return [
                'status' => 400,
                'error' => 'db_create_unique_error',
                'body' => $attributes
            ];
        } catch (ValidationException $e) {
            unset($attributes['audience'], $attributes['csrf_hash']);
            return [
                'status' => 400,
                'error' => 'validation_create_error',
                'validation' => $e->getReport(),
                'body' => $attributes
            ];
        }
    }
}
