<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Action;

use Exception;
use Paneric\AuthApi\Credential\Interfaces\Action\ResetApiTokenApiActionInterface;
use Paneric\ComponentModuleApi\Exceptions\ValidationException;
use Paneric\ComponentModuleApi\Infrastructure\Event\UpdateFailureEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\BeforeUpdateEvent;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Throwable;

class ResetApiTokenApiAction implements ResetApiTokenApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->adapter = $adapter;
        $this->config = $config->update();
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(array $attributes, string $id = null): array
    {
        if ($id === null) {
            return [
                'status' => 401,
                'error' => 'bad_credentials'
            ];
        }

        if (!$attributes['is_active']) {
            return [
                'status' => 400,
                'error' => 'non_activated_credentials'
            ];
        }

        $dto = new $this->config['dto_class']();
        $dto->hydrate($attributes);

        $findOneByCriteria = $this->config['find_one_by_criteria'];
        $updateUniqueCriteria = $this->config['update_unique_criteria'];

        try {
            $this->eventDispatcher->dispatch(new BeforeUpdateEvent($dto));
            $updateResult = $this->adapter->updateUnique(
                $findOneByCriteria($dto, $id),
                $updateUniqueCriteria($id),
                $dto
            );

            if ($updateResult === null) {
                throw new Exception('db_update_unique_error');
            }

            $this->eventDispatcher->dispatch(new UpdateFailureEvent($dto));

            return [
                'status' => 200,
                'body' => ['api_token' => $attributes['api_token'] ?? '']
            ];
        } catch (Throwable $e) {
            $this->eventDispatcher->dispatch(new UpdateFailureEvent($dto));

            return [
                'status' => 400,
                'error' => $e->getMessage(),
                'body' => ['email' => $attributes['email'] ?? '']
            ];
        }
    }
}
