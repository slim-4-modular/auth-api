<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Action;

use Paneric\AuthApi\Credential\Interfaces\Action\ResetPasswordApiActionInterface;
use Paneric\ComponentModuleApi\Exceptions\ValidationException;
use Paneric\ComponentModuleApi\Infrastructure\Event\BeforeUpdateEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\UpdateFailureEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\UpdateSuccessEvent;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

class ResetPasswordApiAction implements ResetPasswordApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected GuardInterface $guard;
    protected EventDispatcherInterface $eventDispatcher;

    protected int $status;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        GuardInterface $guard,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->adapter = $adapter;
        $this->config = $config->update();
        $this->guard = $guard;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(array $attributes, string $routeName, ?string $id = null): ?array
    {
        if ($id === null) {
            return [
                'status' => 405,
                'error' => 'non_existing_user',
                'body' => ['email' => $attributes['email'] ?? '']
            ];
        }

        $dto = new $this->config['dto_class']();
        $dto->hydrate($attributes);

        $findOneByCriteria = $this->config['find_one_by_criteria'];
        $updateUniqueCriteria = $this->config['update_unique_criteria'];

        try {
            (new $this->config['vld_class']())($dto, $routeName);

            $dto->setPasswordHash(
                $this->guard->hashPassword($dto->getPasswordHash())
            );

            $this->eventDispatcher->dispatch(new BeforeUpdateEvent($dto));
            $updateResult = $this->adapter->updateUnique(
                $findOneByCriteria($dto, $id),
                $updateUniqueCriteria($id),
                $dto
            );

            if ($updateResult !== null) {
                $this->eventDispatcher->dispatch(new UpdateSuccessEvent($dto));

                return [
                    'status' => 200,
                    'body' => ['id' => $id],
                ];
            }

            $this->eventDispatcher->dispatch(new UpdateFailureEvent($dto));

            return [
                'status' => 400,
                'error' => 'db_update_unique_error',
                'body' => array_merge(['id' => $id], $attributes)
            ];
        } catch (ValidationException $e) {
            unset($attributes['csrf_hash']);
            return [
                'status' => 400,
                'error' => 'validation_update_error',
                'validation' => $e->getReport(),
                'body' => $attributes
            ];
        }
    }
}
