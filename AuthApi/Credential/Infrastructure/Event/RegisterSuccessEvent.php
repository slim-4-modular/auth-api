<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Infrastructure\Event;

use Paneric\Interfaces\Event\Event;
use Paneric\Interfaces\Event\EventInterface;

class RegisterSuccessEvent extends Event implements EventInterface
{
}
