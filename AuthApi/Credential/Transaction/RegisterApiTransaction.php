<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Transaction;

use Paneric\AuthApi\Credential\Interfaces\Action\RegisterApiActionInterface;
use Paneric\AuthApi\Credential\Interfaces\Transaction\RegisterApiTransactionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\CreateApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\GetOneByApiActionInterface;
use Paneric\DBAL\Manager;
use Paneric\DBAL\Transaction;
use Paneric\Interfaces\Config\ConfigInterface;

class RegisterApiTransaction extends Transaction implements RegisterApiTransactionInterface
{
    private array $config;

    private const USER_ROLE_REF = 'auth-user';

    public function __construct(
        protected Manager $manager,
        protected GetOneByApiActionInterface $getOneRoleByApiAction,
        protected RegisterApiActionInterface $registerCredentialApiAction,
        protected CreateApiActionInterface $createRoleCredentialApiAction,
        ConfigInterface $config
    ) {
        parent::__construct($manager);

        $this->config = ($config())['register'];
    }

    public function __invoke(string $routeName, ?array $attributes): array
    {
        if (empty($attributes)) {
            return [
                'status' => 400,
                'error' => 'invalid_or_missing_arguments',
                'body' => [],
            ];
        }

        $roleConfig = $this->config['role'];
        $credentialConfig = $this->config['credential'];
        $roleCredentialConfig = $this->config['role_credential'];

        $this->adaptManager(
            $roleConfig['table'],
            $roleConfig['dao_class'],
            $roleConfig['fetch_mode'],
        );

        $result = ($this->getOneRoleByApiAction)('ref', self::USER_ROLE_REF);
        if ($result['status'] !== 200) {
            return $result;
        }
        $role = $result['body'];

        $this->manager->beginTransaction();

        $this->adaptManager(
            $credentialConfig['table'],
            $credentialConfig['dao_class'],
            $credentialConfig['fetch_mode'],
        );

        $result = ($this->registerCredentialApiAction)($attributes, $routeName);
        if ($result['status'] !== 201) {
            $this->manager->rollBack();

            return $result;
        }
        $credential = $result['body'];

        $this->adaptManager(
            $roleCredentialConfig['table'],
            $roleCredentialConfig['dao_class'],
            $roleCredentialConfig['fetch_mode'],
        );

        $result = ($this->createRoleCredentialApiAction)(
            [
                'role_id' => $role['id'],
                'credential_id' => $credential['id']
            ],
            $routeName
        );

        if ($result['status'] !== 201) {
            $this->manager->rollBack();

            return $result;
        }

        $this->manager->commit();

        return $result;
    }
}
