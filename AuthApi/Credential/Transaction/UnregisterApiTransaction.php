<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Credential\Transaction;

use Paneric\AuthApi\Credential\Interfaces\Transaction\UnregisterApiTransactionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\DeleteApiActionInterface;
use Paneric\ComponentModuleApi\Interfaces\Action\DeleteByApiActionInterface;
use Paneric\DBAL\Manager;
use Paneric\DBAL\Transaction;
use Paneric\Interfaces\Config\ConfigInterface;

class UnregisterApiTransaction extends Transaction implements UnregisterApiTransactionInterface
{
    private array $config;

    public function __construct(
        protected Manager $manager,
        protected DeleteByApiActionInterface $deleteRoleCredentialByApiAction,
        protected DeleteApiActionInterface $deleteCredentialApiAction,
        ConfigInterface $config
    ) {
        parent::__construct($manager);

        $this->config = ($config())['unregister'];
    }

    public function __invoke(?string $credentialId): array
    {
        if ($credentialId === null) {
            return [
                'status' => 405,
                'error' => 'request_from_non_existing_user'
            ];
        }

        $credentialConfig = $this->config['credential'];
        $roleCredentialConfig = $this->config['role_credential'];

        $this->manager->beginTransaction();

        $this->adaptManager(
            $roleCredentialConfig['table'],
            $roleCredentialConfig['dao_class'],
            $roleCredentialConfig['fetch_mode'],
        );

        $result = ($this->deleteRoleCredentialByApiAction)('credential_id', $credentialId);
        if ($result['status'] !== 200) {
            $this->manager->rollBack();

            return $result;
        }

        $this->adaptManager(
            $credentialConfig['table'],
            $credentialConfig['dao_class'],
            $credentialConfig['fetch_mode'],
        );

        $result = ($this->deleteCredentialApiAction)($credentialId);
        if ($result['status'] !== 200) {
            $this->manager->rollBack();

            return $result;
        }

        $this->manager->commit();

        return $result;
    }
}
