CREATE TABLE `action` (
  `acn_id` int(11) UNSIGNED NOT NULL,
  `acn_ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `acn_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `acn_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `action`
ADD PRIMARY KEY (`acn_id`),
  ADD UNIQUE KEY `acn_ref` (`acn_ref`),
  ADD KEY `acn_created_at` (`acn_created_at`),
  ADD KEY `acn_updated_at` (`acn_updated_at`);

INSERT INTO `action` (`acn_id`, `acn_ref`) VALUES
  (1, 'add'),
  (2, 'remove'),
  (3, 'get-by-id'),
  (4, 'get'),
  (5, 'get-by'),
  (6, 'get-ext'),
  (7, 'get-paginated'),
  (8, 'edit'),
  (9, '*'),
  (10, 'unregister');

ALTER TABLE `action`
  MODIFY `acn_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
