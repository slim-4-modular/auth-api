CREATE TABLE `credential` (
  `crd_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crd_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crd_gsm` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crd_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crd_terms` tinyint(1) NOT NULL DEFAULT '0',
  `crd_is_active` tinyint(1) NOT NULL DEFAULT '0',
  `crd_activation_reset_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `crd_password_reset_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `crd_api_token` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `crd_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
  `crd_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `credential`
  ADD PRIMARY KEY (`crd_id`),
  ADD UNIQUE KEY `crd_email` (`crd_email`),
  ADD UNIQUE KEY `crd_gsm` (`crd_gsm`),
  ADD UNIQUE KEY `crd_password_hash` (`crd_password_hash`),
  ADD KEY `crd_terms` (`crd_terms`),
  ADD KEY `crd_is_active` (`crd_is_active`),
  ADD KEY `crd_activation_reset_id` (`crd_activation_reset_id`),
  ADD KEY `crd_password_reset_id` (`crd_password_reset_id`),
  ADD KEY `crd_created_at` (`crd_created_at`),
  ADD KEY `crd_updated_at` (`crd_updated_at`);

INSERT INTO `credential` (`crd_id`, `crd_email`, `crd_gsm`, crd_password_hash, crd_terms, crd_is_active) VALUES
  ('00000000000000.00000000', 'visitor@visitor.com', '002 123 456','$2y$10$VHhOfHwMevkE2vN37jneieL4XYlhl42BLHDZBrKr00ue5E0SfhnCm', 1, 1),
  ('00000000000000.11111111', 'admin@admin.com', '001 123 456','$2y$10$VqPuuGsHuJP32TKlyOd9BeumyfiQ4koLsLZynshJmNOMud6PpGLD.', 1, 1),
  ('00000000000000.22222222', 'user@user.com', '003 123 456','$2y$10$tREhVkm3AsSBhzTvUeD/leSG4fSc.E1eQiWQVPSTDQt/O.95fiNIe', 1, 1),
  ('00000000000000.33333333', 'creator@creator.com', '004 123 456','$2y$10$Gssd0tefghQOpMNf5SWkKOI9skQhPRH/h5ybCpL1J6cw4F3HVcEPa', 1, 1),
  ('00000000000000.44444444', 'reader@reader.com', '005 123 456','$2y$10$ZpJh4hdoL7HJ0Q11yu8mK.qCtFRKa4CR3VxvHhMXfR/5JtBUHaMwy', 1, 1),
  ('00000000000000.55555555', 'updater@updater.com', '006 123 456','$2y$10$/En6V9z2e4HC5F1ytO9pduL0Djxs5VVw7hK/H4zP06PJxX/IEgdr6', 1, 1),
  ('00000000000000.66666666', 'deleter@deleter.com', '007 123 456','$2y$10$zD8Y8JelnddLKIV4h64qBeSktJIoRGyXNvZC8bWpHD5yXJAL4F83K', 1, 1),
  ('00000000000000.77777777', 'tester@tester.com', '008 123 456','$2y$10$uJ2a4FGz3SJb6bEO7pMSW.a2qsy3OWyvwGYfU1OjqlKZGED1XZWnW', 1, 1);
