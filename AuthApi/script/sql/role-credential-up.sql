CREATE TABLE `role_credential` (
  `rlcr_id` int(11) UNSIGNED NOT NULL,
  `rlcr_role_id` int(11) UNSIGNED NOT NULL,
  `rlcr_credential_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rlcr_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rlcr_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `role_credential`
    ADD PRIMARY KEY (`rlcr_id`),
    ADD UNIQUE KEY `rlcr_role_id_credential_id` (`rlcr_role_id`,`rlcr_credential_id`),
    ADD KEY `rlcr_role_id` (`rlcr_role_id`),
    ADD KEY `rlcr_credential_id` (`rlcr_credential_id`) USING BTREE,
    ADD KEY `rlcr_created_at` (`rlcr_created_at`),
    ADD KEY `rlcr_updated_at` (`rlcr_updated_at`);

INSERT INTO `role_credential` (`rlcr_id`, `rlcr_role_id`, `rlcr_credential_id`) VALUES
  (1, 1, '00000000000000.11111111'),
  (2, 1, '00000000000000.77777777');

ALTER TABLE `role_credential`
  MODIFY `rlcr_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `role_credential`
  ADD CONSTRAINT `role_credential_ibfk_1` FOREIGN KEY (`rlcr_role_id`) REFERENCES `role` (`rle_id`),
  ADD CONSTRAINT `role_credential_ibfk_2` FOREIGN KEY (`rlcr_credential_id`) REFERENCES `credential` (`crd_id`);
