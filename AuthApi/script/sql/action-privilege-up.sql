CREATE TABLE `action_privilege` (
  `acpv_id` int(11) UNSIGNED NOT NULL,
  `acpv_action_id` int(11) UNSIGNED NOT NULL,
  `acpv_privilege_id` int(11) UNSIGNED NOT NULL,
  `acpv_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `acpv_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `action_privilege`
  ADD PRIMARY KEY (`acpv_id`),
  ADD UNIQUE KEY `acpv_action_id_privilege_id` (`acpv_action_id`,`acpv_privilege_id`),
  ADD KEY `acpv_action_id` (`acpv_action_id`),
  ADD KEY `acpv_privilege_id` (`acpv_privilege_id`) USING BTREE,
  ADD KEY `acpv_created_at` (`acpv_created_at`),
  ADD KEY `acpv_updated_at` (`acpv_updated_at`);

INSERT INTO `action_privilege` (`acpv_id`, `acpv_action_id`, `acpv_privilege_id`) VALUES
  (1, 1, 1), (2, 2, 1), (3, 3, 1), (4, 5, 1), (5, 8, 1),
  (6, 1, 2), (7, 2, 2), (8, 4, 2), (9, 5, 2), (10, 6, 2), (11, 7, 2), (12, 8, 2),

  (13, 1, 3), (14, 2, 3), (15, 3, 3), (16, 5, 3), (17, 8, 3),
  (18, 1, 4), (19, 2, 4), (20, 4, 4), (21, 5, 4), (22, 6, 4), (23, 7, 4), (24, 8, 4),

  (25, 1, 5), (26, 2, 5), (27, 3, 5), (28, 5, 5), (29, 8, 5),
  (30, 1, 6), (31, 2, 6), (32, 4, 6), (33, 5, 6), (34, 6, 6), (35, 7, 6), (36, 8, 6),

  (37, 1, 7), (38, 2, 7), (39, 3, 7), (40, 5, 7), (41, 8, 7),
  (42, 1, 8), (43, 2, 8), (44, 4, 8), (45, 5, 8), (46, 6, 8), (47, 7, 8), (48, 8, 8),

  (49, 1, 9), (50, 2, 9), (51, 3, 9), (52, 5, 9), (53, 8, 9),
  (54, 1, 10), (55, 2, 10), (56, 4, 10), (57, 5, 10), (58, 6, 10), (59, 7, 10), (60, 8, 10),

  (61, 1, 11), (62, 2, 11), (63, 3, 11), (64, 5, 11), (65, 8, 11),
  (66, 1, 12), (67, 2, 12), (68, 4, 12), (69, 5, 12), (70, 6, 12), (71, 7, 12), (72, 8, 12),

  (73, 1, 13), (74, 2, 13), (75, 3, 13), (76, 5, 13), (77, 8, 13),
  (78, 1, 14), (79, 2, 14), (80, 4, 14), (81, 5, 14), (82, 6, 14), (83, 7, 14), (84, 8, 14),

  (85, 10, 15);

ALTER TABLE `action_privilege`
  MODIFY `acpv_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

ALTER TABLE `action_privilege`
  ADD CONSTRAINT `action_privilege_ibfk_1` FOREIGN KEY (`acpv_action_id`) REFERENCES `action` (`acn_id`),
  ADD CONSTRAINT `action_privilege_ibfk_2` FOREIGN KEY (`acpv_privilege_id`) REFERENCES `privilege` (`prv_id`);
