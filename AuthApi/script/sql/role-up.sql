CREATE TABLE `role` (
  `rle_id` int(11) UNSIGNED NOT NULL,
  `rle_ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rle_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rle_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `role`
  ADD PRIMARY KEY (`rle_id`),
  ADD UNIQUE KEY `rle_ref` (`rle_ref`),
  ADD KEY `rle_created_at` (`rle_created_at`),
  ADD KEY `rle_updated_at` (`rle_updated_at`);

INSERT INTO `role` (`rle_id`, `rle_ref`) VALUES
  (1, 'auth-admin'),
  (2, 'auth-user'),
  (3, 'auth-creator'),
  (4, 'auth-reader'),
  (5, 'auth-updater'),
  (6, 'auth-deleter'),
  (7, 'auth-visitor');

ALTER TABLE `role`
  MODIFY `rle_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
