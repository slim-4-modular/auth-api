CREATE TABLE `privilege` (
  `prv_id` int(11) UNSIGNED NOT NULL,
  `prv_ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prv_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `prv_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `privilege`
  ADD PRIMARY KEY (`prv_id`),
  ADD UNIQUE KEY `prv_ref` (`prv_ref`),
  ADD KEY `prv_created_at` (`prv_created_at`),
  ADD KEY `prv_updated_at` (`prv_updated_at`);

INSERT INTO `privilege` (`prv_id`, `prv_ref`) VALUES
  (1, 'auth.action'),
  (2, 'auth.actions'),
  (3, 'auth.privilege'),
  (4, 'auth.privileges'),
  (5, 'auth.action-privilege'),
  (6, 'auth.action-privileges'),
  (7, 'auth.role'),
  (8, 'auth.roles'),
  (9, 'auth.privilege-role'),
  (10, 'auth.privilege-roles'),
  (11, 'auth.credential'),
  (12, 'auth.credentials'),
  (13, 'auth.role-credential'),
  (14, 'auth.role-credentials'),
  (15, 'auth.account');

ALTER TABLE `privilege`
  MODIFY `prv_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
