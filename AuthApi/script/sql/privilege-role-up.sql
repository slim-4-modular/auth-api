CREATE TABLE `privilege_role` (
  `pvrl_id` int(11) UNSIGNED NOT NULL,
  `pvrl_privilege_id` int(11) UNSIGNED NOT NULL,
  `pvrl_role_id` int(11) UNSIGNED NOT NULL,
  `pvrl_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pvrl_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `privilege_role`
  ADD PRIMARY KEY (`pvrl_id`),
  ADD UNIQUE KEY `pvrl_privilege_id_role_id` (`pvrl_privilege_id`,`pvrl_role_id`),
  ADD KEY `pvrl_privilege_id` (`pvrl_privilege_id`),
  ADD KEY `pvrl_role_id` (`pvrl_role_id`) USING BTREE,
  ADD KEY `pvrl_created_at` (`pvrl_created_at`),
  ADD KEY `pvrl_updated_at` (`pvrl_updated_at`);

INSERT INTO `privilege_role` (`pvrl_id`, `pvrl_privilege_id`, `pvrl_role_id`) VALUES
  (1, 1, 1),
  (2, 2, 1),
  (3, 3, 1),
  (4, 4, 1),
  (5, 5, 1),
  (6, 6, 1),
  (7, 7, 1),
  (8, 8, 1),
  (9, 9, 1),
  (10, 10, 1),
  (11, 11, 1),
  (12, 12, 1),
  (13, 13, 1),
  (14, 14, 1),
  (15, 15, 2);

ALTER TABLE `privilege_role`
    MODIFY `pvrl_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

ALTER TABLE `privilege_role`
  ADD CONSTRAINT `privilege_role_ibfk_1` FOREIGN KEY (`pvrl_privilege_id`) REFERENCES `privilege` (`prv_id`),
  ADD CONSTRAINT `privilege_role_ibfk_2` FOREIGN KEY (`pvrl_role_id`) REFERENCES `role` (`rle_id`);
