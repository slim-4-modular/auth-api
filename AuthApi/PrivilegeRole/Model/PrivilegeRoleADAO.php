<?php

declare(strict_types=1);

namespace Paneric\AuthApi\PrivilegeRole\Model;

use Paneric\AuthApi\Privilege\Model\PrivilegeDAO;
use Paneric\AuthApi\Role\Model\RoleDAO;
use Paneric\DataObject\ADAO;

class PrivilegeRoleADAO extends ADAO
{
    use PrivilegeRoleTrait;

    protected ?int $id = null;

    protected ?PrivilegeDAO $privilege;
    protected ?RoleDAO $role;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'pvrl_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->privilege = new PrivilegeDAO();
            $this->values = $this->privilege->hydrate($this->values);

            $this->role = new RoleDAO();
            $this->values = $this->role->hydrate($this->values);

            unset($this->values);
        }
    }


    public function getId(): ?int
    {
        return $this->id;
    }
    public function setId(null|int|string $id): void
    {
        if ($id !== null && !is_int($id)) {
            $this->id = (int) $id;
            return;
        }
        $this->id = $id;
    }


    public function getPrivilege(): ?PrivilegeDAO
    {
        return $this->privilege;
    }
    public function getRole(): ?RoleDAO
    {
        return $this->role;
    }

    public function setPrivilege(?PrivilegeDAO $privilege): void
    {
        $this->privilege = $privilege;
    }
    public function setRole(?RoleDAO $role): void
    {
        $this->role = $role;
    }
}
