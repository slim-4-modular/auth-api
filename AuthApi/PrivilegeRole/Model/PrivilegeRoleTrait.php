<?php

declare(strict_types=1);

namespace Paneric\AuthApi\PrivilegeRole\Model;

trait PrivilegeRoleTrait
{
    protected ?int $privilegeId;
    protected ?int $roleId;


    public function getPrivilegeId(): ?int
    {
        return $this->privilegeId;
    }
    public function getRoleId(): ?int
    {
        return $this->roleId;
    }


    public function setPrivilegeId(null|int|string $privilegeId): void
    {
        if ($privilegeId !== null && !is_int($privilegeId)) {
            $this->privilegeId = (int) $privilegeId;
            return;
        }
        $this->privilegeId = $privilegeId;
    }
    public function setRoleId(null|int|string $roleId): void
    {
        if ($roleId !== null && !is_int($roleId)) {
            $this->roleId = (int) $roleId;
            return;
        }
        $this->roleId = $roleId;
    }
}
