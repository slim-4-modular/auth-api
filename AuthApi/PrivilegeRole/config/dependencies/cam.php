<?php

declare(strict_types=1);

use Paneric\AuthApi\PrivilegeRole\config\ModuleConfig;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;

return array_merge(
    (array) require './../vendor/paneric/component-module-api/src/config/dependencies/cam.php',
    [
        ModuleConfigInterface::class => static function (): ModuleConfig {
            return new ModuleConfig();
        },
    ]
);
