<?php

declare(strict_types=1);

namespace Paneric\AuthApi\PrivilegeRole\config;

use Paneric\AuthApi\PrivilegeRole\Model\PrivilegeRoleADAO;
use Paneric\AuthApi\PrivilegeRole\Model\PrivilegeRoleDAO;
use Paneric\AuthApi\PrivilegeRole\Model\PrivilegeRoleVLD;
use Paneric\ComponentModuleApi\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use PDO;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'privilege_role';
    protected string $prefix = 'pvrl_';

    protected array $uniqueKeys = ['privilege_id', 'role_id'];//unique key

    protected string $adaoClass = PrivilegeRoleADAO::class;
    protected string $daoClass = PrivilegeRoleDAO::class;
    protected string $vldClass = PrivilegeRoleVLD::class;

    protected bool $stringId = false;

    protected string $query = "
        SELECT * 
        FROM privilege_role AS pvrl
            LEFT JOIN privilege AS prv ON pvrl.pvrl_privilege_id = prv.prv_id
            LEFT JOIN role AS rle ON pvrl.pvrl_role_id = rle.rle_id
    ";

    public function repository(): array
    {
        return [
            'table' => $this->table,
            'dto_class' => $this->adaoClass,
            'select_query' => $this->query,
            'fetch_mode' => PDO::FETCH_CLASS,
        ];
    }
}
