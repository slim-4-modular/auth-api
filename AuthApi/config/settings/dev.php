<?php

declare(strict_types=1);

use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;

return [
    'pagination-middleware' => [
        'auth.actions.get-paginated' => ModuleRepositoryInterface::class,//middleware
        'auth.privileges.get-paginated' => ModuleRepositoryInterface::class,//middleware
        'auth.action-privileges.get-paginated' => ModuleRepositoryInterface::class,//middleware
        'auth.roles.get-paginated' => ModuleRepositoryInterface::class,//middleware
        'auth.privilege-roles.get-paginated' => ModuleRepositoryInterface::class,//middleware
        'auth.credentials.get-paginated' => ModuleRepositoryInterface::class,//middleware
        'auth.role-credentials.get-paginated' => ModuleRepositoryInterface::class,//middleware

        'page_rows_number' => 5,//middleware
    ],
];
