<?php

declare(strict_types=1);

namespace Paneric\AuthApi\EventSubscriber\Traits;

use Paneric\AuthApi\Credential\Model\CredentialDAO;
use Paneric\Interfaces\Event\EventInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

trait EventSubscriberMailerTrait
{
    private function setProcessId(EventInterface $event, string $process): void
    {
        /** @var CredentialDAO $credential */
        $credential = $event->getData();
        $credential->{'set' . ucfirst($process) . 'ResetId'}(
            $this->mailerService->setProcessId()
        );

        $this->eventLogger->info(sprintf(
            'Credential %s ID created for %s.',
            $process,
            $credential->getEmail()
        ));
    }

    private function sendProcessLink(EventInterface $event, string $process): void
    {
        /** @var CredentialDAO $credential */
        $credential = $event->getData();

        try {
            $this->mailerService->sendProcessLink(
                $process . '_reset',
                $credential->{'get' . ucfirst($process) . 'ResetId'}(),
                $credential->getId(),
                $credential->getEmail(),
            );
        } catch (Exception | TransportExceptionInterface $e) {
            $this->eventLogger->error($e->getMessage());
        }

        $this->eventLogger->info(sprintf(
            'Credential %s link sent for %s.',
            $process,
            $credential->getEmail()
        ));
    }
}
