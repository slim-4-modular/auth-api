<?php

declare(strict_types=1);

namespace Paneric\AuthApi\EventSubscriber\Service;

use Paneric\Interfaces\Guard\GuardInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Email;

class MailerService
{
    public function __construct(
        private readonly GuardInterface $guard,
        private readonly array $config,
        private readonly string $local
    ) {
    }

    public function setProcessId(): string
    {
        return $this->guard->generateRandomString($this->config['random_string_length']);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendProcessLink(string $process, string $processId, string $userId, string $recipient): void
    {
        $messages = $this->config['mail_content'];

        $email = (new Email())
            ->from($this->config['sender'])
            ->to($recipient)
            ->subject($messages[$process . '_email_subject'][$this->local])
            ->text(sprintf(
                $messages[$process . '_email_message'][$this->local],
                $this->config[$process . '_time_delay'],
                $this->config[$process . '_url'],
                urlencode($userId),
                $this->guard->hash($processId)
            ));

        $transport = Transport::fromDsn(sprintf(
            $this->config['dsn'],
            $this->config['smtp_user'],
            $this->config['smtp_password']
        ));
        $mailer = new Mailer($transport);
        $mailer->send($email);
    }
}
