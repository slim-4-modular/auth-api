<?php

declare(strict_types=1);

namespace Paneric\AuthApi\EventSubscriber;

use Paneric\ComponentModuleApi\Infrastructure\Event\CreateFailureEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\CreateSuccessEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\DeleteFailureEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\DeleteSuccessEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\UpdateFailureEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\UpdateSuccessEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\BeforeCreateEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\BeforeDeleteEvent;
use Paneric\ComponentModuleApi\Infrastructure\Event\BeforeUpdateEvent;
use Paneric\Logger\EventLogger;

class CrudEventSubscriber
{
    public function __construct(readonly private EventLogger $eventLogger)
    {
    }

    public function onBeforeCreate(BeforeCreateEvent $event): void
    {
        $this->eventLogger->info('JEST BEFORE CREATE');
    }
    public function onCreateFailure(CreateFailureEvent $event): void
    {
    }
    public function onCreateSuccess(CreateSuccessEvent $event): void
    {
    }

    public function onBeforeDelete(BeforeDeleteEvent $event): void
    {
    }
    public function onDeleteFailure(DeleteFailureEvent $event): void
    {
    }
    public function onDeleteSuccess(DeleteSuccessEvent $event): void
    {
    }

    public function onBeforeUpdate(BeforeUpdateEvent $event): void
    {
    }
    public function onUpdateFailure(UpdateFailureEvent $event): void
    {
    }
    public function onUpdateSuccess(UpdateSuccessEvent $event): void
    {
    }
}
