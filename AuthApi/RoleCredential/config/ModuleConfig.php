<?php

declare(strict_types=1);

namespace Paneric\AuthApi\RoleCredential\config;

use Paneric\AuthApi\RoleCredential\Model\RoleCredentialADAO;
use Paneric\AuthApi\RoleCredential\Model\RoleCredentialDAO;
use Paneric\AuthApi\RoleCredential\Model\RoleCredentialVLD;
use Paneric\ComponentModuleApi\Action\Config\ModuleMultiConfigTrait;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;
use PDO;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleMultiConfigTrait;

    protected string $table = 'role_credential';
    protected string $prefix = 'rlcr_';

    protected array $uniqueKeys = ['role_id', 'credential_id'];//unique key

    protected string $adaoClass = RoleCredentialADAO::class;
    protected string $daoClass = RoleCredentialDAO::class;
    protected string $vldClass = RoleCredentialVLD::class;

    protected bool $stringId = false;

    protected string $query = "
        SELECT * 
        FROM role_credential AS rlcr
            LEFT JOIN role AS rle ON rlcr.rlcr_role_id = rle.rle_id
            LEFT JOIN credential AS crd ON rlcr.rlcr_credential_id = crd.crd_id
    ";

    public function repository(): array
    {
        return [
            'table' => $this->table,
            'dto_class' => $this->adaoClass,
            'select_query' => $this->query,
            'fetch_mode' => PDO::FETCH_CLASS,
        ];
    }
}
