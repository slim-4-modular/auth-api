<?php

declare(strict_types=1);

namespace Paneric\AuthApi\RoleCredential\Model;

trait RoleCredentialTrait
{
    protected ?int $roleId;
    protected ?string $credentialId;


    public function getRoleId(): ?int
    {
        return $this->roleId;
    }
    public function getCredentialId(): ?string
    {
        return $this->credentialId;
    }


    public function setRoleId(null|int|string $roleId): void
    {
        if ($roleId !== null && !is_int($roleId)) {
            $this->roleId = (int) $roleId;
            return;
        }
        $this->roleId = $roleId;
    }
    public function setCredentialId(null|string $credentialId): void
    {
        $this->credentialId = $credentialId;
    }
}
