<?php

declare(strict_types=1);

namespace Paneric\AuthApi\RoleCredential\Model;

use Paneric\ComponentModuleApi\Model\Interfaces\ValidatorInterface;
use Paneric\ComponentModuleApi\Model\Validator;

class RoleCredentialVLD extends Validator implements ValidatorInterface
{
    protected function validate(array $attributes, int $index = null): void
    {
    }
}
