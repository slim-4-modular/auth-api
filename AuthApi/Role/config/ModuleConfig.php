<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Role\config;

use Paneric\AuthApi\Role\Model\RoleVLD;
use Paneric\AuthApi\Role\Model\RoleDAO;
use Paneric\ComponentModuleApi\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'role';
    protected string $prefix = 'rle_';

    protected array $uniqueKeys = ['ref'];//unique keys

    protected string $daoClass = RoleDAO::class;
    protected string $vldClass = RoleVLD::class;

    protected bool $stringId = false;
}
