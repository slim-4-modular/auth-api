<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Action\config;

use Paneric\AuthApi\Action\Model\ActionVLD;
use Paneric\AuthApi\Action\Model\ActionDAO;
use Paneric\ComponentModuleApi\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'action';
    protected string $prefix = 'acn_';

    protected array $uniqueKeys = ['ref'];//unique keys

    protected string $daoClass = ActionDAO::class;
    protected string $vldClass = ActionVLD::class;

    protected bool $stringId = false;
}
