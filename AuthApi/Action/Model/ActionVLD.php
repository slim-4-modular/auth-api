<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Action\Model;

use Paneric\ComponentModuleApi\Model\Interfaces\ValidatorInterface;
use Paneric\ComponentModuleApi\Model\Validator;
use Webmozart\Assert\Assert;
use Webmozart\Assert\InvalidArgumentException;

class ActionVLD extends Validator implements ValidatorInterface
{
    protected function validate(array $attributes, int $index = null): void
    {
        try {
            Assert::notEmpty($attributes['ref'], 'Expected a non-empty value. Got: %s');
        } catch (InvalidArgumentException $e) {
            if ($index === null) {
                $this->messages['ref'] = $e->getMessage();
            } else {
                $this->messages['ref'][$index] = $e->getMessage();
            }
        }
    }
}
