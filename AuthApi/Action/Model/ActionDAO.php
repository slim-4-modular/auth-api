<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Action\Model;

use Paneric\DataObject\DAO;

class ActionDAO extends DAO
{
    use ActionTrait;

    protected ?int $id = null;
    protected string $ref;
    protected string $pl;
    protected string $en;

    public function __construct()
    {
        $this->prefix = 'acn_';

        $this->setMaps();
    }


    public function getId(): ?int
    {
        return $this->id;
    }
    public function getRef(): string
    {
        return $this->ref;
    }
    public function getPl(): string
    {
        return $this->pl;
    }
    public function getEn(): string
    {
        return $this->en;
    }


    public function setId(null|int|string $id): void
    {
        if ($id !== null && !is_int($id)) {
            $this->id = (int) $id;
            return;
        }
        $this->id = $id;
    }
    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
    public function setPl(string $pl): void
    {
        $this->pl = $pl;
    }
    public function setEn(string $en): void
    {
        $this->en = $en;
    }
}
