<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Migrations;

use Exception;
use Paneric\Migrations\AbstractMigration;

final class Migration20220101000001Action extends AbstractMigration
{
    private const SQL_PATH = 'AuthApi/script/sql/action-up.sql';

    public function getDescription(): string
    {
        return '';
    }

    /**
     * @throws Exception
     */
    public function execute(): void
    {
        $this->executeQuery(file_get_contents(self::SQL_PATH));
    }
}
