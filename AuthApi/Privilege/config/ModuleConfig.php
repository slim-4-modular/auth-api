<?php

declare(strict_types=1);

namespace Paneric\AuthApi\Privilege\config;

use Paneric\AuthApi\Privilege\Model\PrivilegeVLD;
use Paneric\AuthApi\Privilege\Model\PrivilegeDAO;
use Paneric\ComponentModuleApi\Action\Config\ModuleSingleConfigTrait;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleSingleConfigTrait;

    protected string $table = 'privilege';
    protected string $prefix = 'prv_';

    protected array $uniqueKeys = ['ref'];//unique keys

    protected string $daoClass = PrivilegeDAO::class;
    protected string $vldClass = PrivilegeVLD::class;

    protected bool $stringId = false;
}
