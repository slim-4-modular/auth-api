<?php

declare(strict_types=1);

namespace Paneric\AuthApi\ActionCredential\Model;

use Paneric\AuthApi\ActionRole\Model\ActionRoleADAO;
use Paneric\AuthApi\Credential\Model\CredentialDAO;
use Paneric\DataObject\ADAO;

class ActionCredentialADAO extends ADAO
{
    use ActionCredentialTrait;

    protected ?int $id = null;

    protected ?ActionRoleADAO $actionRole;
    protected ?CredentialDAO $credential;

    public function __construct(array $values = null)
    {
        parent::__construct($values);

        $this->prefix = 'accr_';

        $this->setMaps();

        if ($this->values) {
            $this->values = $this->hydrate($this->values);

            $this->actionRole = new ActionRoleADAO($this->values);

            $this->credential = new CredentialDAO();
            $this->values = $this->credential->hydrate($this->values);

            unset($this->values);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function setId(null|int|string $id): void
    {
        if ($id !== null && !is_int($id)) {
            $this->id = (int) $id;
            return;
        }
        $this->id = $id;
    }


    public function getActionRole(): ?ActionRoleADAO
    {
        return $this->actionRole;
    }
    public function getCredential(): ?CredentialDAO
    {
        return $this->credential;
    }

    public function setActionRole(?ActionRoleADAO $actionRole): void
    {
        $this->actionRole = $actionRole;
    }
    public function setCredential(?CredentialDAO $credential): void
    {
        $this->credential = $credential;
    }
}
