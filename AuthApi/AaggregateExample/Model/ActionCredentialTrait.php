<?php

declare(strict_types=1);

namespace Paneric\AuthApi\ActionCredential\Model;

trait ActionCredentialTrait
{
    protected ?int $actionRoleId;
    protected ?string $credentialId;


    public function getActionRoleId(): ?int
    {
        return $this->actionRoleId;
    }
    public function getCredentialId(): ?string
    {
        return $this->credentialId;
    }


    public function setActionRoleId(null|int|string $actionRoleId): void
    {
        if ($actionRoleId !== null && !is_int($actionRoleId)) {
            $this->actionRoleId = (int) $actionRoleId;
            return;
        }
        $this->actionRoleId = $actionRoleId;
    }
    public function setCredentialId(?string $credentialId): void
    {
        $this->credentialId = $credentialId;
    }
}
