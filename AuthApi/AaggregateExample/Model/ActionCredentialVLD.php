<?php

declare(strict_types=1);

namespace Paneric\AuthApi\ActionCredential\Model;

use Paneric\ComponentModuleApi\Exceptions\ValidationException;
use Paneric\ComponentModuleApi\Interfaces\ValidatorInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\DataObjectInterface;

class ActionCredentialVLD implements ValidatorInterface
{
    /**
     * @throws ValidationException
     */
    public function invoke(DataObjectInterface|array $dao): void
    {
        throw new ValidationException();
    }
}
