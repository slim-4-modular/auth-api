<?php

declare(strict_types=1);

namespace Tests\Integration\AuthApi\Credential\Action;

use PHPUnit\Framework\TestCase;
use Tests\Integration\AuthApi\Traits\DtoTestTrait;
use Tests\Integration\AuthApi\Traits\TestTrait;

class CredentialTest extends TestCase
{
    use TestTrait;
    use DtoTestTrait;

    protected array $headers;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->setHeaders();
    }

    public function getAllProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/credentials/get',
            ],
        ];
    }

    public function getOneByIdProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/credential/get/%s',
                '00000000000000.00000000',
                'x.x',
                'resource_not_found',
            ],
        ];
    }

    public function addProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/credential/add',
                [
                    'email' => 'mail_test_add@credential.com',
                    'gsm' => '001 001 001',
                    'password_hash' => 'password_hash_test_add',
                ],
                'db_create_unique_error',
                'http://localhost:81/auth-api/credential/remove/%s',
            ],
        ];
    }

    public function addsProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/credentials/add',
                [
                    'email' => [
                        'mail_test_add_1@credential.com',
                        'mail_test_add_2@credential.com',
                    ],
                    'gsm' => [
                        '001 001 001',
                        '001 001 002',
                    ],
                    'password_hash' => [
                        'password_hash_test_add_1',
                        'password_hash_test_add_2',
                    ],
                ],
                [
                    'email' => [
                        'mail_test_add_2@credential.com',
                        'mail_test_add_3@credential.com',
                    ],
                    'gsm' => [
                        '001 001 002',
                        '001 001 003',
                    ],
                    'password_hash' => [
                        'password_hash_test_add_2',
                        'password_hash_test_add_3',
                    ],
                ],
                'email',
                'mail_test_add_2@credential.com',
                'http://localhost:81/auth-api/credentials/remove',
                [
                    'email' => [
                        'mail_test_add_1@credential.com',
                        'mail_test_add_2@credential.com',
                        'mail_test_add_3@credential.com',
                    ]
                ],
            ],
        ];
    }

    public function updateProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/credential/add',
                [
                    'email' => 'mail_test_add@credential.com',
                    'gsm' => '001 001 001',
                    'password_hash' => 'password_hash_test_add',
                ],
                'http://localhost:81/auth-api/credential/edit/%s',
                [
                    'email' => 'mail_test_update@credential.com',
                    'gsm' => '001 001 001',
                    'password_hash' => 'password_hash_test_update',
                ],
                'http://localhost:81/auth-api/credential/remove/%s',
            ],
        ];
    }

    public function updatesProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/credentials/add',
                [
                    'email' => [
                        'mail_test_add_1@credential.com',
                        'mail_test_add_2@credential.com',
                    ],
                    'gsm' => [
                        '001 001 001',
                        '001 001 002',
                    ],
                    'password_hash' => [
                        'password_hash_test_add_1',
                        'password_hash_test_add_2',
                    ],
                ],
                'http://localhost:81/auth-api/credentials/edit',
                [
                    'email' => [
                        'mail_test_update_1@credential.com',
                        'mail_test_update_2@credential.com',
                    ],
                    'gsm' => [
                        '001 001 001',
                        '001 001 002',
                    ],
                    'password_hash' => [
                        'password_hash_test_update_1',
                        'password_hash_test_update_2',
                    ],
                ],
                'http://localhost:81/auth-api/credentials/remove',
            ],
        ];
    }
}
