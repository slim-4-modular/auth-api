<?php

declare(strict_types=1);

namespace Tests\Integration\AuthApi\Credential\Account;

use Paneric\ComponentModuleApi\Model\Interfaces\ModulePersisterInterface;
use Paneric\ComponentModuleApi\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\Interfaces\Guard\GuardInterface;
use PDO;
use PHPUnit\Framework\TestCase;
use Tests\Integration\AuthApi\Traits\TestTrait;

class AccountTest extends TestCase
{
    use AccountTrait;
    use TestTrait;

    protected GuardInterface $guard;
    protected ModuleRepositoryInterface $repository;
    protected ModulePersisterInterface $persister;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->setGuard();
        $this->setRepository();
        $this->setPersister();
    }

    protected array $dbalConfig = [
        'limit' => 10,
        'host' => 'localhost:3306',
        'charset' => 'utf8',
        'dbName' => 'auth-api',
        'user' => 'resu',
        'password' => 'toor',
        'options' => [
            PDO::ATTR_PERSISTENT         => true,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::MYSQL_ATTR_FOUND_ROWS => true//count rowCounts even if identical values updated
        ],
    ];

    public function accountProvider(): array
    {
        return [
            [
                // registerData
                [
                    'http://localhost:81/auth-api/account/register',
                    [
                        'email' => 'account@account.com',
                        'gsm' => '001 001 001',
                        'password_hash' => 'account',
                        'terms' => 1,
                        'is_active' => 0,
                    ],
                ],
                // activateData
                [
                    'email',
                    'account@account.com',
                    'http://localhost:81/auth-api/account/activate/%s/%s',
                ],
                // requestsActivationResetData
                [
                    'http://localhost:81/auth-api/account/request-activation-reset',
                    [
                        'email' => 'account@account.com',
                        'password_hash' => 'account',
                    ],
                ],
                // requestsPasswordResetData
                [
                    'http://localhost:81/auth-api/account/request-password-reset',
                    [
                        'email' => 'account@account.com',
                    ],
                    'http://localhost:81/auth-api/account/reset-password/%s/%s',
                    [
                        'password_hash' => 'account_reset',
                    ],
                ],
                // resetApiTokenData
                [
                    'http://localhost:81/auth-api/account/reset-api-token',
                    [
                        'email' => 'account@account.com',
                        'password_hash' => 'account_reset',
                        'audience' => 'PhpUnit',
                    ],
                ],
                // unregisterData
                [
                    'http://localhost:81/auth-api/account/unregister',
                    [
                        'email' => 'account@account.com',
                        'password_hash' => 'account_reset',
                    ],
                    'Authorization: Bearer %s',
                ],
            ],
        ];
    }
}
