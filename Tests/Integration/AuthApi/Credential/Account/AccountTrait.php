<?php

declare(strict_types=1);

namespace Tests\Integration\AuthApi\Credential\Account;

use JsonException;
use Paneric\AuthApi\Credential\Model\CredentialDAO;
use Paneric\ComponentModuleApi\Infrastructure\ModulePersister;
use Paneric\ComponentModuleApi\Infrastructure\ModuleRepository;
use Paneric\DBAL\DataPreparator;
use Paneric\DBAL\Manager;
use Paneric\DBAL\PDOBuilder;
use Paneric\DBAL\Persister;
use Paneric\DBAL\QueryBuilder;
use Paneric\DBAL\SequencePreparator;
use RandomLib\Factory as RandomFactory;
use Paneric\Guard\Guard;
use Paneric\AuthApi\Credential\config\ModuleConfig as CredentialModuleConfig;

trait AccountTrait
{
    /**
     * @dataProvider accountProvider
     *
     * @throws JsonException
     */
    public function testIfAccount(
        array $registerData,
        array $activateData,
        array $requestsActivationResetData,
        array $requestsPasswordResetData,
        array $resetApiTokenData,
        array $unregisterData
    ): void {
        $this->ifRegisters($registerData);

        $credential = $this->ifActivates($activateData);

        $this->ifRequestsActivationReset($requestsActivationResetData, $credential);

        $credential = $this->ifRequestsPasswordReset($requestsPasswordResetData);

        $apiToken = $this->itResetsApiToken($activateData, $resetApiTokenData, $credential);

        $this->ifUnregisters($unregisterData, $apiToken);
    }

    /**
     * @throws JsonException
     */
    private function ifRegisters(array $registerData): void
    {
        [$url, $payLoad] = $registerData;

        $response = $this->getCurlResponse('POST', $url, $payLoad);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(201, $response['status']);
    }

    /**
     * @throws JsonException
     */
    private function ifActivates(array $activateData): CredentialDAO
    {
        [$field, $value, $url] = $activateData;

        /** @var CredentialDAO $credential */
        $credential = $this->repository->findOneBy(['crd_' . $field => $value]);

        $this->assertEquals(0, $credential->getIsActive());

        $credentialId = $credential->getId();
        $activationResetId = $credential->getActivationResetId();
        $hash = $this->guard->hash($activationResetId);

        $url = sprintf($url, $credentialId, $hash);

        $response = $this->getCurlResponse('GET', $url);
        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(200, $response['status']);
        $this->assertEquals($value, $response['body'][$field]);

        /** @var CredentialDAO $credential */
        $credential = $this->repository->findOneBy(['crd_' . $field => $value]);

        $this->assertEquals(1, $credential->getIsActive());

        return $credential;
    }

    /**
     * @throws JsonException
     */
    private function ifRequestsActivationReset(array $requestsActivationResetData, CredentialDAO $credential): void
    {
        $credentialId = $credential->getId();
        $attributes = $credential->convert();
        $attributes['is_active'] = 0;
        unset($attributes['id'], $attributes['created_at'], $attributes['updated_at']);

        $credential = new CredentialDAO();
        $credential->hydrate($attributes);
        /** @var Persister $persister */
        $this->persister->adaptManager();
        $this->persister->update(['crd_id' => $credentialId], $credential);

        [$url, $payLoad] = $requestsActivationResetData;

        $response = $this->getCurlResponse('POST', $url, $payLoad);
        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(200, $response['status']);
        $this->assertEquals($payLoad['email'], $response['body']['email']);
    }

    /**
     * @throws JsonException
     */
    private function ifRequestsPasswordReset(array $requestsPasswordResetData): CredentialDAO
    {
        [$url1, $payLoad1, $url2, $payLoad2] = $requestsPasswordResetData;

        $response = $this->getCurlResponse('POST', $url1, $payLoad1);
        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(200, $response['status']);
        $this->assertEquals($payLoad1['email'], $response['body']['email']);

        /** @var CredentialDAO $credential */
        $credential = $this->repository->findOneBy(['crd_email' => $payLoad1['email']]);

        $credentialId = $credential->getId();
        $passwordResetId = $credential->getPasswordResetId();
        $hash = $this->guard->hash($passwordResetId);

        $url2 = sprintf($url2, $credentialId, $hash);

        $response = $this->getCurlResponse('POST', $url2, $payLoad2);
        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(200, $response['status']);

        return $credential;
    }

    /**
     * @throws JsonException
     */
    private function itResetsApiToken(
        array $activateData,
        array $requestApiTokenData,
        CredentialDAO $credential
    ): string {
        $credentialId = $credential->getId();
        $activationResetId = $credential->getActivationResetId();
        $hash = $this->guard->hash($activationResetId);

        [$field, $value, $url1] = $activateData;

        $url1 = sprintf($url1, $credentialId, $hash);

        $response = $this->getCurlResponse('GET', $url1);
        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(200, $response['status']);
        $this->assertEquals($value, $response['body'][$field]);

        [$url, $payLoad] = $requestApiTokenData;

        $response = $this->getCurlResponse('POST', $url, $payLoad);
        $this->assertEquals(200, $response['status']);

        return $response['body']['api_token'];
    }

    /**
     * @throws JsonException
     */
    private function ifUnregisters(array $unregisterData, $apiToken): void
    {
        [$url, $payLoad, $authorizationHeader] = $unregisterData;
        $headers[] = sprintf($authorizationHeader, $apiToken);

        $response = $this->getCurlResponse('POST', $url, $payLoad, $headers);
        $this->assertEquals(200, $response['status']);
    }

    protected function setGuard(): void
    {
        $randomFactory = new RandomFactory();

        $this->guard = new Guard(
            $randomFactory->getMediumStrengthGenerator(),
            [
                'key_ascii' => 'def000008d147968b763eac2453624272c423c77347bd423e736328d648365fb243ea2130953f36ff38b5c9d8b15e5063df9e014010a3e06a1ec60612e54737a9f54f45d',
                'algo_hash' => 'sha512',
                'algo_password' => PASSWORD_BCRYPT,
                'options_algo_password' => [
                    'cost' => 10,
                ],
            ]
        );
    }

    protected function setRepository(): void
    {
        $manager = new Manager(
            (new PDOBuilder())->build($this->dbalConfig),
            new QueryBuilder(new SequencePreparator()),
            new DataPreparator()
        );

        $this->repository = new ModuleRepository(
            $manager,
            new CredentialModuleConfig()
        );
    }

    protected function setPersister(): void
    {
        $manager = new Manager(
            (new PDOBuilder())->build($this->dbalConfig),
            new QueryBuilder(new SequencePreparator()),
            new DataPreparator()
        );

        $this->persister = new ModulePersister(
            $manager,
            new CredentialModuleConfig()
        );
    }
}
