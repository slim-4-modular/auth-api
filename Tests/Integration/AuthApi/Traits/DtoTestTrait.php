<?php

declare(strict_types=1);

namespace Tests\Integration\AuthApi\Traits;

use JsonException;

trait DtoTestTrait
{
    /**
     * @dataProvider getAllProvider
     *
     * @throws JsonException
     */
    public function testIfGetsAll(string $url1): void
    {
        $response = $this->getCurlResponse('GET', $url1, null, $this->headers);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('body', $response);

        $this->assertEquals(200, $response['status']);
    }

    /**
     * @dataProvider getOneByIdProvider
     *
     * @throws JsonException
     */
    public function testIfGetsOneById(string $url1, string|int $id1, string|int $id2, string $error): void
    {
        $url = sprintf($url1, urlencode((string) $id1));

        $response = $this->getCurlResponse('GET', $url, null, $this->headers);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('body', $response);

        $this->assertEquals(200, $response['status']);

        $url = sprintf($url1, urlencode((string) $id2));

        $response = $this->getCurlResponse('GET', $url, null, $this->headers);

        $this->assertArrayHasKey('status', $response);
        $this->assertArrayHasKey('error', $response);

        $this->assertEquals(400, $response['status']);
        $this->assertEquals($error, $response['error']);
    }

    /**
     * @dataProvider addProvider
     *
     * @throws JsonException
     */
    public function testIfAdds(
        string $url1,
        array $payLoad1,
        string $error,
        string $url2
    ): void {
        $response = $this->getCurlResponse('POST', $url1, $payLoad1, $this->headers);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(201, $response['status']);
        $id = $response['body']['id'];

        $response = $this->getCurlResponse('POST', $url1, $payLoad1, $this->headers);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(400, $response['status']);

        $this->assertArrayHasKey('error', $response);
        $this->assertEquals($error, $response['error']);

        $url2 = sprintf($url2, urlencode((string) $id));
        $response = $this->getCurlResponse('DELETE', $url2, null, $this->headers);
        $this->assertEquals(200, $response['status']);
    }

    /**
     * @dataProvider addsProvider
     * @param string[] $fieldValues
     *
     * @throws JsonException
     */
    public function testIfAddss(
        string $url1,
        array $payLoad1,
        array $payLoad2,
        string $field,
        int|string $fieldValue2,
        string $url2,
        array $fieldValues,
    ): void {
        $response = $this->getCurlResponse('POST', $url1, $payLoad1, $this->headers);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(201, $response['status']);

        $response = $this->getCurlResponse('POST', $url1, $payLoad2, $this->headers);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(400, $response['status']);

        $this->assertArrayHasKey('error', $response);
        $this->assertEquals('db_create_multiple_error', $response['error']);

        $this->assertArrayHasKey('body', $response);

        $this->assertEquals($fieldValue2, $response['body'][0][$field]);

        $response = $this->getCurlResponse('DELETE', $url2, $fieldValues, $this->headers);
        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(200, $response['status']);
    }

    /**
     * @dataProvider updateProvider
     *
     * @throws JsonException
     */
    public function testIfUpdates(
        string $url1,
        array $payLoad1,
        string $url2,
        array $payLoad2,
        string $url3,
    ): void {
        $response = $this->getCurlResponse('POST', $url1, $payLoad1, $this->headers);
        $this->assertEquals(201, $response['status']);

        $id = $response['body']['id'];
        $url2 = sprintf($url2, $id);
        $response = $this->getCurlResponse('PUT', $url2, $payLoad2, $this->headers);

        $this->assertEquals(200, $response['status']);
        $this->assertEquals($id, $response['body']['id']);

        $url3 = sprintf($url3, $id);
        $response = $this->getCurlResponse('DELETE', $url3, null, $this->headers);
        $this->assertEquals(200, $response['status']);
        $this->assertEquals($id, $response['body']['id']);
    }

    /**
     * @dataProvider updatesProvider
     *
     * @throws JsonException
     */
    public function testIfUpdatess(
        string $url1,
        array $payLoad1,
        string $url2,
        array $payLoad2,
        string $url3,
    ): void {
        $response = $this->getCurlResponse('POST', $url1, $payLoad1, $this->headers);
        $this->assertEquals(201, $response['status']);

        $ids = $response['body'];
        $payLoad2 = array_merge($payLoad2, $ids);
        $response = $this->getCurlResponse('PUT', $url2, $payLoad2, $this->headers);
        $this->assertEquals(200, $response['status']);
        $this->assertEquals($ids, $response['body']);

        $response = $this->getCurlResponse('DELETE', $url3, $ids, $this->headers);
        $this->assertEquals(200, $response['status']);
        $this->assertEquals($ids, $response['body']);
    }

    /**
     * @throws JsonException
     */
    protected function setHeaders(): void
    {
        $url = 'http://localhost:81/auth-api/account/reset-api-token';
        $payLoad = [
            'email' => 'tester@tester.com',
            'password_hash' => 'tester',
            'audience' => 'PhpUnit',
        ];
        $response = $this->getCurlResponse('POST', $url, $payLoad);

        $this->headers[] = sprintf('Authorization: Bearer %s', $response['body']['api_token']);
    }
}
