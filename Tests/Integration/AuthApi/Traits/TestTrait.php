<?php

declare(strict_types=1);

namespace Tests\Integration\AuthApi\Traits;

use JsonException;

trait TestTrait
{
    /**
     * @throws JsonException
     */
    protected function getCurlResponse(
        string $method,
        string $url,
        array $payLoad = null,
        array $headers = null
    ): array {
        $conn = curl_init();
        curl_setopt($conn, CURLOPT_URL, $url);
        curl_setopt($conn, CURLOPT_RETURNTRANSFER, true);
        if ($payLoad !== null) {
            curl_setopt($conn, CURLOPT_POSTFIELDS, http_build_query($payLoad));
        }
        curl_setopt($conn, CURLOPT_CUSTOMREQUEST, $method);

        if ($headers !== null) {
            curl_setopt($conn, CURLOPT_HTTPHEADER, $headers);
        }

        $dudu = curl_exec($conn);
//dump($dudu);
        return json_decode($dudu, true, 512, JSON_THROW_ON_ERROR);
    }
}
