<?php

declare(strict_types=1);

namespace Tests\Integration\AuthApi\Privilege\Action;

use PHPUnit\Framework\TestCase;
use Tests\Integration\AuthApi\Traits\DtoTestTrait;
use Tests\Integration\AuthApi\Traits\TestTrait;

class PrivilegeTest extends TestCase
{
    use TestTrait;
    use DtoTestTrait;

    protected array $headers;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->setHeaders();
    }

    public function getAllProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/privileges/get',
            ],
        ];
    }

    public function getOneByIdProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/privilege/get/%s',
                1,
                99999999999,
                'resource_not_found',
            ],
        ];
    }

    public function addProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/privilege/add',
                [
                    'ref' => 'privilege_test_add',
                ],
                'db_create_unique_error',
                'http://localhost:81/auth-api/privilege/remove/%s',
            ],
        ];
    }

    public function addsProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/privileges/add',
                [
                    'ref' => [
                        'privilege_test_add_1',
                        'privilege_test_add_2',
                    ],
                ],
                [
                    'ref' => [
                        'privilege_test_add_2',
                        'privilege_test_add_3',
                    ],
                ],
                'ref',
                'privilege_test_add_2',
                'http://localhost:81/auth-api/privileges/remove',
                [
                    'ref' => [
                        'privilege_test_add_1',
                        'privilege_test_add_2',
                        'privilege_test_add_3',
                    ]
                ],
            ],
        ];
    }

    public function updateProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/privilege/add',
                [
                    'ref' => 'privilege_test_add',
                ],
                'http://localhost:81/auth-api/privilege/edit/%s',
                [
                    'ref' => 'privilege_test_update',
                ],
                'http://localhost:81/auth-api/privilege/remove/%s',
            ],
        ];
    }

    public function updatesProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/privileges/add',
                [
                    'ref' => [
                        'privilege_test_add_1',
                        'privilege_test_add_2',
                    ],
                ],
                'http://localhost:81/auth-api/privileges/edit',
                [
                    'ref' => [
                        'privilege_test_update_1',
                        'privilege_test_update_2',
                    ],
                ],
                'http://localhost:81/auth-api/privileges/remove',
            ],
        ];
    }
}
