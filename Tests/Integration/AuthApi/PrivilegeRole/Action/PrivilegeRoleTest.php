<?php

declare(strict_types=1);

namespace Tests\Integration\AuthApi\PrivilegeRole\Action;

use PHPUnit\Framework\TestCase;
use Tests\Integration\AuthApi\Traits\DtoTestTrait;
use Tests\Integration\AuthApi\Traits\TestTrait;

class PrivilegeRoleTest extends TestCase
{
    use TestTrait;
    use DtoTestTrait;

    protected array $headers;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->setHeaders();
    }

    public function getAllProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/privilege-roles/get',
            ],
        ];
    }

    public function getOneByIdProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/privilege-role/get/%s',
                1,
                99999999999,
                'resource_not_found',
            ],
        ];
    }

    public function addProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/privilege-role/add',
                [
                    'privilege_id' => 1,
                    'role_id' => 7,
                ],
                'db_create_unique_error',
                'http://localhost:81/auth-api/privilege-role/remove/%s',
            ],
        ];
    }

    public function addsProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/privilege-roles/add',
                [
                    'privilege_id' => [
                        1,
                        2,
                    ],
                    'role_id' => [
                        7,
                        7,
                    ],
                ],
                [
                    'privilege_id' => [
                        2,
                        3,
                    ],
                    'role_id' => [
                        7,
                        7,
                    ],
                ],
                'privilege_id',
                2,
                'http://localhost:81/auth-api/privilege-roles/remove',
                [
                    'privilege_id' => [
                        1,
                        2,
                        3,
                    ],
                    'role_id' => [
                        7,
                        7,
                        7,
                    ],
                ],
            ],
        ];
    }

    public function updateProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/privilege-role/add',
                [
                    'privilege_id' => 1,
                    'role_id' => 7
                ],
                'http://localhost:81/auth-api/privilege-role/edit/%s',
                [
                    'privilege_id' => 2,
                    'role_id' => 7
                ],
                'http://localhost:81/auth-api/privilege-role/remove/%s',
            ],
        ];
    }

    public function updatesProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/privilege-roles/add',
                [
                    'privilege_id' => [
                        1,
                        2,
                    ],
                    'role_id' => [
                        7,
                        7,
                    ],
                ],
                'http://localhost:81/auth-api/privilege-roles/edit',
                [
                    'privilege_id' => [
                        3,
                        4,
                    ],
                    'role_id' => [
                        7,
                        7,
                    ],
                ],
                'http://localhost:81/auth-api/privilege-roles/remove',
            ],
        ];
    }
}
