<?php

declare(strict_types=1);

namespace Tests\Integration\AuthApi\Action\Action;

use PHPUnit\Framework\TestCase;
use Tests\Integration\AuthApi\Traits\DtoTestTrait;
use Tests\Integration\AuthApi\Traits\TestTrait;

class ActionTest extends TestCase
{
    use TestTrait;
    use DtoTestTrait;

    protected array $headers;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->setHeaders();
    }

    public function getAllProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/actions/get',
            ],
        ];
    }

    public function getOneByIdProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/action/get/%s',
                1,
                9999999999,
                'resource_not_found',
            ],
        ];
    }

    public function addProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/action/add',
                [
                    'ref' => 'action_test_add',
                ],
                'db_create_unique_error',
                'http://localhost:81/auth-api/action/remove/%s',
            ],
        ];
    }

    public function addsProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/actions/add',
                [
                    'ref' => [
                        'action_test_add_1',
                        'action_test_add_2',
                    ],
                ],
                [
                    'ref' => [
                        'action_test_add_2',
                        'action_test_add_3',
                    ],
                ],
                'ref',
                'action_test_add_2',
                'http://localhost:81/auth-api/actions/remove',
                [
                    'ref' => [
                        'action_test_add_1',
                        'action_test_add_2',
                        'action_test_add_3',
                    ]
                ],
            ],
        ];
    }

    public function updateProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/action/add',
                [
                    'ref' => 'action_test_add',
                ],
                'http://localhost:81/auth-api/action/edit/%s',
                [
                    'ref' => 'action_test_update',
                ],
                'http://localhost:81/auth-api/action/remove/%s',
            ],
        ];
    }

    public function updatesProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/actions/add',
                [
                    'ref' => [
                        'action_test_add_1',
                        'action_test_add_2',
                    ],
                ],
                'http://localhost:81/auth-api/actions/edit',
                [
                    'ref' => [
                        'action_test_update_1',
                        'action_test_update_2',
                    ],
                ],
                'http://localhost:81/auth-api/actions/remove',
            ],
        ];
    }
}
