<?php

declare(strict_types=1);

namespace Tests\Integration\AuthApi\Role\Action;

use PHPUnit\Framework\TestCase;
use Tests\Integration\AuthApi\Traits\DtoTestTrait;
use Tests\Integration\AuthApi\Traits\TestTrait;

class RoleTest extends TestCase
{
    use TestTrait;
    use DtoTestTrait;

    protected array $headers;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->setHeaders();
    }

    public function getAllProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/roles/get',
            ],
        ];
    }

    public function getOneByIdProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/role/get/%s',
                1,
                99999999999,
                'resource_not_found',
            ],
        ];
    }

    public function addProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/role/add',
                [
                    'ref' => 'role_test_add',
                ],
                'db_create_unique_error',
                'http://localhost:81/auth-api/role/remove/%s',
            ],
        ];
    }

    public function addsProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/roles/add',
                [
                    'ref' => [
                        'role_test_add_1',
                        'role_test_add_2',
                    ],
                ],
                [
                    'ref' => [
                        'role_test_add_2',
                        'role_test_add_3',
                    ],
                ],
                'ref',
                'role_test_add_2',
                'http://localhost:81/auth-api/roles/remove',
                [
                    'ref' => [
                        'role_test_add_1',
                        'role_test_add_2',
                        'role_test_add_3',
                    ],
                ],
            ],
        ];
    }

    public function updateProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/role/add',
                [
                    'ref' => 'role_test_add',
                ],
                'http://localhost:81/auth-api/role/edit/%s',
                [
                    'ref' => 'role_test_update',
                ],
                'http://localhost:81/auth-api/role/remove/%s',
            ],
        ];
    }

    public function updatesProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/roles/add',
                [
                    'ref' => [
                        'role_test_add_1',
                        'role_test_add_2',
                    ],
                ],
                'http://localhost:81/auth-api/roles/edit',
                [
                    'ref' => [
                        'role_test_update_1',
                        'role_test_update_2',
                    ],
                ],
                'http://localhost:81/auth-api/roles/remove',
            ],
        ];
    }
}
