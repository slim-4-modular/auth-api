<?php

declare(strict_types=1);

namespace Tests\Integration\AuthApi\ActionPrivilege\Action;

use PHPUnit\Framework\TestCase;
use Tests\Integration\AuthApi\Traits\DtoTestTrait;
use Tests\Integration\AuthApi\Traits\TestTrait;

class ActionPrivilegeTest extends TestCase
{
    use TestTrait;
    use DtoTestTrait;

    protected array $headers;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->setHeaders();
    }

    public function getAllProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/action-privileges/get',
            ],
        ];
    }

    public function getOneByIdProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/action-privilege/get/%s',
                1,
                99999999999,
                'resource_not_found',
            ],
        ];
    }

    public function addProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/action-privilege/add',
                [
                    'action_id' => 9,
                    'privilege_id' => 1,
                ],
                'db_create_unique_error',
                'http://localhost:81/auth-api/action-privilege/remove/%s',
            ],
        ];
    }

    public function addsProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/action-privileges/add',
                [
                    'action_id' => [
                        9,
                        9,
                    ],
                    'privilege_id' => [
                        1,
                        2,
                    ],
                ],
                [
                    'action_id' => [
                        9,
                        9,
                    ],
                    'privilege_id' => [
                        2,
                        3,
                    ],
                ],
                'privilege_id',
                2,
                'http://localhost:81/auth-api/action-privileges/remove',
                [
                    'action_id' => [
                        9,
                        9,
                        9,
                    ],
                    'privilege_id' => [
                        1,
                        2,
                        3,
                    ],
                ],
            ],
        ];
    }

    public function updateProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/action-privilege/add',
                [
                    'action_id' => 9,
                    'privilege_id' => 1
                ],
                'http://localhost:81/auth-api/action-privilege/edit/%s',
                [
                    'action_id' => 9,
                    'privilege_id' => 2
                ],
                'http://localhost:81/auth-api/action-privilege/remove/%s',
            ],
        ];
    }

    public function updatesProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/action-privileges/add',
                [
                    'action_id' => [
                        9,
                        9,
                    ],
                    'privilege_id' => [
                        1,
                        2,
                    ],
                ],
                'http://localhost:81/auth-api/action-privileges/edit',
                [
                    'action_id' => [
                        9,
                        9,
                    ],
                    'privilege_id' => [
                        3,
                        4,
                    ],
                ],
                'http://localhost:81/auth-api/action-privileges/remove',
            ],
        ];
    }
}
