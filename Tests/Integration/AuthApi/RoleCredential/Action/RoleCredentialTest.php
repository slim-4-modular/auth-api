<?php

declare(strict_types=1);

namespace Tests\Integration\AuthApi\RoleCredential\Action;

use PHPUnit\Framework\TestCase;
use Tests\Integration\AuthApi\Traits\DtoTestTrait;
use Tests\Integration\AuthApi\Traits\TestTrait;

class RoleCredentialTest extends TestCase
{
    use TestTrait;
    use DtoTestTrait;

    protected array $headers;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->setHeaders();
    }

    public function getAllProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/role-credentials/get',
            ],
        ];
    }

    public function getOneByIdProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/role-credential/get/%s',
                1,
                99999999999,
                'resource_not_found',
            ],
        ];
    }

    public function addProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/role-credential/add',
                [
                    'role_id' => 7,
                    'credential_id' => '00000000000000.00000000',
                ],
                'db_create_unique_error',
                'http://localhost:81/auth-api/role-credential/remove/%s',
            ],
        ];
    }

    public function addsProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/role-credentials/add',
                [
                    'role_id' => [
                        7,
                        7,
                    ],
                    'credential_id' => [
                        '00000000000000.11111111',
                        '00000000000000.22222222',
                    ],
                ],
                [
                    'role_id' => [
                        7,
                        7,
                    ],
                    'credential_id' => [
                        '00000000000000.22222222',
                        '00000000000000.33333333',
                    ],
                ],
                'credential_id',
                '00000000000000.22222222',
                'http://localhost:81/auth-api/role-credentials/remove',
                [
                    'role_id' => [
                        7,
                        7,
                        7,
                    ],
                    'credential_id' => [
                        '00000000000000.11111111',
                        '00000000000000.22222222',
                        '00000000000000.33333333',
                    ],
                ],
            ],
        ];
    }

    public function updateProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/role-credential/add',
                [
                    'role_id' => 7,
                    'credential_id' => '00000000000000.00000000'
                ],
                'http://localhost:81/auth-api/role-credential/edit/%s',
                [
                    'role_id' => 7,
                    'credential_id' => '00000000000000.11111111',
                ],
                'http://localhost:81/auth-api/role-credential/remove/%s',
            ],
        ];
    }

    public function updatesProvider(): array
    {
        return [
            [
                'http://localhost:81/auth-api/role-credentials/add',
                [
                    'role_id' => [
                        7,
                        7,
                    ],
                    'credential_id' => [
                        '00000000000000.11111111',
                        '00000000000000.22222222',
                    ],
                ],
                'http://localhost:81/auth-api/role-credentials/edit',
                [
                    'role_id' => [
                        7,
                        7,
                    ],
                    'credential_id' => [
                        '00000000000000.33333333',
                        '00000000000000.44444444',
                    ],
                ],
                'http://localhost:81/auth-api/role-credentials/remove',
            ],
        ];
    }
}
