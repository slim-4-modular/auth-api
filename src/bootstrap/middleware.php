<?php

declare(strict_types=1);

use DI\DependencyException;
use DI\NotFoundException;
use Paneric\JWT\JWTAuthorizationMiddleware;
use Paneric\JWT\JWTMiddleware;
use Paneric\Logger\ErrorLogger;
use Paneric\Middleware\RouteMiddleware;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

if (isset($slim, $container)) {
    try {
        $slim->add(JWTAuthorizationMiddleware::class);//always before UriMiddleware
        $slim->add(JWTMiddleware::class);//always before UriMiddleware
        $slim->add(RouteMiddleware::class);//always before UriMiddleware
        $slim->addBodyParsingMiddleware();
        $slim->addRoutingMiddleware();
        $errorMiddleware = $slim->addErrorMiddleware(
            $_ENV['ENV'] === 'dev' || $_ENV['ENV'] === 'test', //false for PROD environment
            true,
            true,
            $container->get(ErrorLogger::class)
        );
        require './../src/bootstrap/error-handlers.php';
    } catch (DependencyException | NotFoundException | NotFoundExceptionInterface | ContainerExceptionInterface $e) {
        echo $e->getMessage();
    }
}
