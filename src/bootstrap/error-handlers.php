<?php

declare(strict_types=1);

use DI\DependencyException;
use DI\NotFoundException;
use Paneric\Slim\Exception\Handler\Api\HttpBadRequestExceptionApiHandler;
use Paneric\Slim\Exception\Handler\Api\HttpExceptionApiHandler;
use Paneric\Slim\Exception\Handler\Api\HttpNotFoundExceptionApiHandler;
use Paneric\Slim\Exception\Handler\Api\HttpUnauthorizedExceptionApiHandler;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Paneric\Slim\Exception\HttpBadRequestException;
use Paneric\Slim\Exception\HttpException;
use Paneric\Slim\Exception\HttpNotFoundException;
use Paneric\Slim\Exception\HttpUnauthorizedException;

if (isset($errorMiddleware, $container)) {
    try {
        $errorMiddleware->setErrorHandler(
            HttpException::class,
            $container->get(HttpExceptionApiHandler::class)
        );

        $errorMiddleware->setErrorHandler(
            HttpBadRequestException::class,
            $container->get(HttpBadRequestExceptionApiHandler::class)
        );

        $errorMiddleware->setErrorHandler(
            HttpNotFoundException::class,
            $container->get(HttpNotFoundExceptionApiHandler::class)
        );

        $errorMiddleware->setErrorHandler(
            HttpUnauthorizedException::class,
            $container->get(HttpUnauthorizedExceptionApiHandler::class)
        );
    } catch (
        DependencyException |
        NotFoundException |
        NotFoundExceptionInterface |
        ContainerExceptionInterface $e
    ) {
    }
}
