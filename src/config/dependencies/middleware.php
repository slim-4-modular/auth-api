<?php

declare(strict_types=1);

use App\config\JWTConfig;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\JWT\JWTAuthorizationMiddleware;
use Paneric\JWT\JWTMiddleware;
use Paneric\Middleware\RouteMiddleware;
use Paneric\Pagination\PaginationMiddleware;
use Psr\Container\ContainerInterface;

return [
    RouteMiddleware::class => static function (ContainerInterface $container): RouteMiddleware {
        return new RouteMiddleware($container);
    },
    PaginationMiddleware::class => static function (ContainerInterface $container): PaginationMiddleware {
        return new PaginationMiddleware(
            $container
        );
    },
    JWTMiddleware::class => static function (ContainerInterface $container): JWTMiddleware {
        return new JWTMiddleware(
            $container->get(JWTConfig::class),
            $container->get(GuardInterface::class),
        );
    },
    JWTAuthorizationMiddleware::class => static function (ContainerInterface $container): JWTAuthorizationMiddleware {
        return new JWTAuthorizationMiddleware(
            $container->get(JWTConfig::class)
        );
    },
];
