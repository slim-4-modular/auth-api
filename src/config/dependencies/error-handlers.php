<?php

declare(strict_types=1);

use Paneric\Logger\ErrorLogger;
use Paneric\Slim\Exception\Handler\Api\HttpBadRequestExceptionApiHandler;
use Paneric\Slim\Exception\Handler\Api\HttpExceptionApiHandler;
use Paneric\Slim\Exception\Handler\Api\HttpNotFoundExceptionApiHandler;
use Paneric\Slim\Exception\Handler\Api\HttpUnauthorizedExceptionApiHandler;
use Psr\Container\ContainerInterface;

return [
    HttpExceptionApiHandler::class => static function (
        ContainerInterface $container
    ): HttpExceptionApiHandler {
        return new HttpExceptionApiHandler(
            $container->get(ErrorLogger::class)
        );
    },

    HttpBadRequestExceptionApiHandler::class => static function (
        ContainerInterface $container
    ): HttpBadRequestExceptionApiHandler {
        return new HttpBadRequestExceptionApiHandler(
            $container->get(ErrorLogger::class)
        );
    },

    HttpNotFoundExceptionApiHandler::class => static function (
        ContainerInterface $container
    ): HttpNotFoundExceptionApiHandler {
        return new HttpNotFoundExceptionApiHandler(
            $container->get(ErrorLogger::class)
        );
    },

    HttpUnauthorizedExceptionApiHandler::class => static function (
        ContainerInterface $container
    ): HttpUnauthorizedExceptionApiHandler {
        return new HttpUnauthorizedExceptionApiHandler(
            $container->get(ErrorLogger::class)
        );
    },
];
