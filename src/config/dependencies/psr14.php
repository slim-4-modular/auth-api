<?php

declare(strict_types=1);

use Crell\Tukio\Dispatcher;
use Crell\Tukio\OrderedListenerProvider;
use Paneric\AuthApi\Credential\EventSubscriber\CredentialEventSubscriber;
use Paneric\AuthApi\EventSubscriber\CrudEventSubscriber;
use Psr\Container\ContainerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

return [
    EventDispatcherInterface::class => static function (ContainerInterface $container): EventDispatcherInterface {
        $provider = new OrderedListenerProvider($container);
        $provider->addSubscriber(CrudEventSubscriber::class, CrudEventSubscriber::class);
        $provider->addSubscriber(CredentialEventSubscriber::class, CredentialEventSubscriber::class);

        return new Dispatcher($provider);
    },
];
