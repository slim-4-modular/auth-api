<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Factory\AppFactory;
use Slim\Interfaces\RouteParserInterface;

return [
    App::class => static function (ContainerInterface $container) {
        return AppFactory::createFromContainer($container);
    },

    'base-path' => static function (ContainerInterface $container) {
        return $container
            ->get(App::class)
            ->getBasePath();
    },

    RouteParserInterface::class => static function (ContainerInterface $container) {
        return $container
            ->get(App::class)
            ->getRouteCollector()
            ->getRouteParser();
    },
];
