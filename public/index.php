<?php

declare(strict_types=1);

use Dotenv\Dotenv;

require __DIR__ . '/../vendor/autoload.php';

$dotEnv = Dotenv::createImmutable('./../');
$dotEnv->load();

require './../src/bootstrap/bootstrap.php';
