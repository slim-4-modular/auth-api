-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: auth-api-mysql
-- Generation Time: Nov 16, 2022 at 03:11 PM
-- Server version: 5.7.14
-- PHP Version: 8.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `auth-api`
--

-- --------------------------------------------------------

--
-- Table structure for table `action`
--

CREATE TABLE `action` (
  `acn_id` int(11) UNSIGNED NOT NULL,
  `acn_ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `acn_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `acn_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `action_privilege`
--

CREATE TABLE `action_privilege` (
  `acpv_id` int(11) UNSIGNED NOT NULL,
  `acpv_action_id` int(11) UNSIGNED NOT NULL,
  `acpv_privilege_id` int(11) UNSIGNED NOT NULL,
  `acpv_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `acpv_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `credential`
--

CREATE TABLE `credential` (
  `crd_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `crd_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `crd_gsm` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `crd_password_hash` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `crd_terms` tinyint(1) NOT NULL DEFAULT '0',
  `crd_is_active` tinyint(1) NOT NULL DEFAULT '0',
  `crd_activation_reset_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `crd_password_reset_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `crd_api_token` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `crd_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
  `crd_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `privilege`
--

CREATE TABLE `privilege` (
  `prv_id` int(11) UNSIGNED NOT NULL,
  `prv_ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prv_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `prv_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `privilege_role`
--

CREATE TABLE `privilege_role` (
  `pvrl_id` int(11) UNSIGNED NOT NULL,
  `pvrl_privilege_id` int(11) UNSIGNED NOT NULL,
  `pvrl_role_id` int(11) UNSIGNED NOT NULL,
  `pvrl_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pvrl_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `rle_id` int(11) UNSIGNED NOT NULL,
  `rle_ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rle_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rle_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_credential`
--

CREATE TABLE `role_credential` (
  `rlcr_id` int(11) UNSIGNED NOT NULL,
  `rlcr_role_id` int(11) UNSIGNED NOT NULL,
  `rlcr_credential_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rlcr_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rlcr_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`acn_id`),
  ADD UNIQUE KEY `acn_ref` (`acn_ref`),
  ADD KEY `acn_created_at` (`acn_created_at`),
  ADD KEY `acn_updated_at` (`acn_updated_at`);

--
-- Indexes for table `action_privilege`
--
ALTER TABLE `action_privilege`
  ADD PRIMARY KEY (`acpv_id`),
  ADD UNIQUE KEY `acpv_privilege_id` (`acpv_privilege_id`) USING BTREE,
  ADD KEY `acpv_action_id` (`acpv_action_id`),
  ADD KEY `acpv_created_at` (`acpv_created_at`),
  ADD KEY `acpv_updated_at` (`acpv_updated_at`);

--
-- Indexes for table `credential`
--
ALTER TABLE `credential`
  ADD PRIMARY KEY (`crd_id`),
  ADD UNIQUE KEY `crd_email` (`crd_email`),
  ADD UNIQUE KEY `crd_gsm` (`crd_gsm`),
  ADD UNIQUE KEY `crd_password_hash` (`crd_password_hash`),
  ADD KEY `crd_terms` (`crd_terms`),
  ADD KEY `crd_is_active` (`crd_is_active`),
  ADD KEY `crd_activation_reset_id` (`crd_activation_reset_id`),
  ADD KEY `crd_password_reset_id` (`crd_password_reset_id`),
  ADD KEY `crd_created_at` (`crd_created_at`),
  ADD KEY `crd_updated_at` (`crd_updated_at`);

--
-- Indexes for table `privilege`
--
ALTER TABLE `privilege`
  ADD PRIMARY KEY (`prv_id`),
  ADD UNIQUE KEY `prv_ref` (`prv_ref`),
  ADD KEY `prv_created_at` (`prv_created_at`),
  ADD KEY `prv_updated_at` (`prv_updated_at`);

--
-- Indexes for table `privilege_role`
--
ALTER TABLE `privilege_role`
  ADD PRIMARY KEY (`pvrl_id`),
  ADD UNIQUE KEY `pvrl_role_id` (`pvrl_role_id`) USING BTREE,
  ADD KEY `pvrl_created_at` (`pvrl_created_at`),
  ADD KEY `pvrl_updated_at` (`pvrl_updated_at`),
  ADD KEY `pvrl_privilege_id` (`pvrl_privilege_id`) USING BTREE;

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`rle_id`),
  ADD UNIQUE KEY `rle_ref` (`rle_ref`),
  ADD KEY `rle_created_at` (`rle_created_at`),
  ADD KEY `rle_updated_at` (`rle_updated_at`);

--
-- Indexes for table `role_credential`
--
ALTER TABLE `role_credential`
  ADD PRIMARY KEY (`rlcr_id`),
  ADD UNIQUE KEY `rlcr_credential_id` (`rlcr_credential_id`) USING BTREE,
  ADD KEY `rlcr_created_at` (`rlcr_created_at`),
  ADD KEY `rlcr_updated_at` (`rlcr_updated_at`),
  ADD KEY `rlcr_role_id` (`rlcr_role_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action`
--
ALTER TABLE `action`
  MODIFY `acn_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `action_privilege`
--
ALTER TABLE `action_privilege`
  MODIFY `acpv_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `privilege`
--
ALTER TABLE `privilege`
  MODIFY `prv_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `privilege_role`
--
ALTER TABLE `privilege_role`
  MODIFY `pvrl_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `rle_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `role_credential`
--
ALTER TABLE `role_credential`
  MODIFY `rlcr_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `action_privilege`
--
ALTER TABLE `action_privilege`
  ADD CONSTRAINT `action_privilege_ibfk_1` FOREIGN KEY (`acpv_action_id`) REFERENCES `action` (`acn_id`),
  ADD CONSTRAINT `action_privilege_ibfk_2` FOREIGN KEY (`acpv_privilege_id`) REFERENCES `privilege` (`prv_id`);

--
-- Constraints for table `privilege_role`
--
ALTER TABLE `privilege_role`
  ADD CONSTRAINT `privilege_role_ibfk_1` FOREIGN KEY (`pvrl_privilege_id`) REFERENCES `privilege` (`prv_id`),
  ADD CONSTRAINT `privilege_role_ibfk_2` FOREIGN KEY (`pvrl_role_id`) REFERENCES `role` (`rle_id`),
  ADD CONSTRAINT `privilege_role_ibfk_3` FOREIGN KEY (`pvrl_privilege_id`) REFERENCES `action_privilege` (`acpv_privilege_id`);

--
-- Constraints for table `role_credential`
--
ALTER TABLE `role_credential`
  ADD CONSTRAINT `role_credential_ibfk_1` FOREIGN KEY (`rlcr_role_id`) REFERENCES `role` (`rle_id`),
  ADD CONSTRAINT `role_credential_ibfk_2` FOREIGN KEY (`rlcr_credential_id`) REFERENCES `credential` (`crd_id`),
  ADD CONSTRAINT `role_credential_ibfk_3` FOREIGN KEY (`rlcr_role_id`) REFERENCES `privilege_role` (`pvrl_role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
