```shell
$ apt search openssh-clinet # installer but usually already installed 
$ ssh tomasz@172.105.7.26

$ which ssh
/usr/bin/ssh

$ cd ~/.ssh
~/.ssh$ 

~/.ssh$ touch config
~/.ssh$ ls
config  id_ed25519  id_ed25519.pub  id_rsa  id_rsa.pub  known_hosts
~/.ssh$ nano config
```
*~/.ssh/config*
```
Host myserver
  Hostname 172.105.7.26
  Port 22
  User tomasz
```

```shell
$ ssh myserver #instead of ip address
```

### SSH key
```shell
~/.ssh$ cd ..
$ ssh-keygen
Genration public/private rsa key pair.
Enter file in which to save the key (/home/tomasz/.ssh/id_rsa) :
```
> `~/.ssh/id_rsa` file with private key  
> `~/.ssh/id_rsa.pub` file with public key

### SSH public key transfer to remote
```shell
$ ssh-copy-id -d ~/.ssh/id_rsa.pub tomasz@172.105.7.26
```
