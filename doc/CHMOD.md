[CHMOD](https://www.pluralsight.com/blog/it-ops/linux-file-permissions)

```shell
$ chmod +rwx #filename to add permissions
$ chmod -rwx #directoryname to remove permissions
$ chmod +x #filename to allow executable permissions
$ chmod -wx #filename to take out write and executable permissions

$ chmod g+w #filename
$ chmod g-wx #filename
$ chmod o+w #filename
$ chmod o-rwx #foldername

$ chgrp groupname filename
$ chgrp groupname foldername
```

