### Export database
```shell
$ mysqldump -u [user] -p[password]`cat /etc/psa/.psa.shadow` [dbname] > [dbname.sql]
```
or zip:
```shell
$ mysqldump -u [user] -p[password] [db_name] | gzip > [dbname.sql.gz] 
```


### Import database
```shell
$ mysql -u [user] -p[password`cat /etc/psa/.psa.shadow` [dbname] < [dbname.sql]
```
or
```shell
$ mysql -u [user] -p[password`cat /etc/psa/.psa.shadow` dbname -e '[dbname.sql]'
```
or unzip
```shell
$ gunzip < [dbname.sql.gz]  | mysql -u [user] -p[password] [dbname] 
```
### PDO Errors messages
```php
if (!$stmt) {
    echo "\nPDO::errorInfo():\n";
    var_dump($this->pdo->errorInfo());
    die;
}
```
### Aggregates and Entities
```php
PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, // DAO
PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS, //ADAO
PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
```


